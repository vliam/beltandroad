// Copyright 2013 Beego Samples authors
//
// Licensed under the Apache License, Version 2.0 (the "License"): you may
// not use this file except in compliance with the License. You may obtain
// a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
// License for the specific language governing permissions and limitations
// under the License.

package controllers

import (
	"encoding/json"
	"net/http"

	"beltAndRoad/models"

	"github.com/astaxie/beego"
	"github.com/gorilla/websocket"

	"fmt"
	"time"
)

// WebSocketController handles WebSocket requests.
type WebSocketController struct {
	BaseController
}

const writeWait = 1 * time.Second
// @Title Join
// @Description create object
// @Param	uname		path 	string	true		""
// @Success 200 {string}
// @Failure 403 body is empty
// @router /join [get]
func (this *WebSocketController) Join() {
	var nInQue uint
	u, _ := this.CheckSession(true)
	//Join chat room.
	if u == nil {
		beego.Error("ERR_USER_WHEN_JOIN")
		return
	}

	// Upgrade from http request to WebSocket.
	ws, err := websocket.Upgrade(this.Ctx.ResponseWriter, this.Ctx.Request, nil, 1024, 1024)
	if _, ok := err.(websocket.HandshakeError); ok {
		http.Error(this.Ctx.ResponseWriter, "Not a websocket handshake", 400)
		return
	} else if err != nil {
		beego.Error("Cannot setup WebSocket connection:", err)
		return
	}

	oJoin := models.MsgCmd{MesId: models.MSG_USER_JOIN, SessionId: u.Sid, Username: u.Username}
	beego.Info("oJoin:", oJoin)
	Join(u.Sid, ws, u.Wid+"_"+u.Username)
	defer Leave(u.Sid)
	cmdQue <- oJoin

	nInQue = 1
	if n, ok :=mInQueMsgNumber.Load(u.Sid); ok {
		nInQue = n.(uint) +1
	}
	mInQueMsgNumber.Store(u.Sid, nInQue)

	//ws.send welcome msg
	// Message receive loop.
	for {
		_, p, err := ws.ReadMessage()
		if err != nil {
			//ws.Close()
			//mSubscribers.Delete(u.Sid)
			Leave(u.Sid)
			//beego.Error("Quit: read error", u.Sid)
			return
		}
		if n, ok :=mInQueMsgNumber.Load(u.Sid); ok {
			if n.(uint)> QueNumLimit{
				beego.Error(u.Sid, "req too many times")
				continue // not check cmd
			}
		}
		//n is small
		oMsg, sErr := this.CheckCommand(string(p))

		if oMsg != nil {
			nInQue = 1
			if n, ok :=mInQueMsgNumber.Load(u.Sid); ok {
				nInQue = n.(uint) +1
			}
			mInQueMsgNumber.Store(u.Sid, nInQue)
			cmdQue <- *oMsg
		} else {
			privateQue <- newEvent(models.EVENT_MESSAGE, u.Sid, sErr)
		}
	}
	beego.Error("Finished for.", u.Sid)
	Leave(u.Sid)
}
func (this *WebSocketController) CheckCommand(msg string) (*models.MsgCmd, string) {
	//{"mesId":4224,"sessionId":"33CB24669151D28A034F6AF16A326A45","data":{"lotteryCode":"BOMB","rate":2,"amount":10,"currency":"USD"}}
	//>>>{"data":{"amount":10,"issue":"2018031402264","index":2,"username":"frank"},"mesId":41}
	//{"mesId":34,"sessionId":"33CB24669151D28A034F6AF16A326A45","data":{"name":"cancel_bet"}}
	//>>>{"data":{"code":1},"mesId":33}
	//{"mesId":422,"sessionId":"33CB24669151D28A034F6AF16A326A45","data":{"cash_out":""}}

	//{"data":{"name":"game_tick","gameTick":16774,"odds":"3.61",},"mesId":35}
	//{"data":{"amount":10,"stopped_at":2,"username":"t2_frank"},"mesId":32}
	//{"data":{"elapsed":20582,"gameCrash":"4.36","forced":true,"bonuses":{}},"mesId":39}
	//{"data":{"name":"game_starting","issue":"2018031402252","maxWin":"0","timeTillStart":5},"mesId":42}
	//{"data":{"name":"game_started","issue":"2018031402252","bets":{},"maxAmount":0},"mesId":31}

	//here yo we check what msg it's
	//place cancel
	//place bet
	//order list

	oMsg := &models.MsgCmd{}
	_ = json.Unmarshal([]byte(msg), oMsg)
	user, _ := this.CheckSession(true)
	if user != nil {
		oMsg.SessionId = user.Sid
		oMsg.Userid = user.UserId
		oMsg.Username = user.Username
		oMsg.Wid = user.Wid
		beego.Debug(user,"CMD:", oMsg )
	} else {
		beego.Error("ERROR_SESSION")
		return nil, "ERROR_SESSION"
	}
	switch oMsg.MesId {
	//valid order
	case models.MSG_USER_BET:
		if oMsg.BetData.LotteryCode != beego.AppConfig.String("bomb::lotteryCode") {
			beego.BeeLogger.Error("ERROR_LOTTERY")
			sRet := models.NewReturnMsg(nil, models.MSG_PRI_BET_OK, "ERROR_LOTTERY", "0004")
			return nil, sRet
		}
		dMaxAmount, _ := beego.AppConfig.Float("bomb::maxAmount")
		dMinAmount, _ := beego.AppConfig.Float("bomb::minAmount")
		dMaxRate, _ := beego.AppConfig.Float("bomb::maxRate")

		if oMsg.BetData.Amount > dMaxAmount {
			beego.Error("AMOUNT too big")
			sRet := models.NewReturnMsg(nil, models.MSG_PRI_BET_OK,  fmt.Sprint(dMaxAmount), "0012")
			return nil, sRet
		}
		if oMsg.BetData.Amount < dMinAmount {
			beego.Error("AMOUNT too small")
			sRet := models.NewReturnMsg(nil, models.MSG_PRI_BET_OK, fmt.Sprint(dMinAmount), "0002")
			return nil, sRet
		}

		if oMsg.BetData.Rate <= 1  {
			beego.Error("INVALID RATE")
			sRet := models.NewReturnMsg(nil, models.MSG_PRI_BET_OK, "Invalid Odds", "0003")
			return nil, sRet
		}

		if oMsg.BetData.Rate > dMaxRate {
			beego.Error("RATE too big")
			sRet := models.NewReturnMsg(nil, models.MSG_PRI_BET_OK, fmt.Sprint(dMaxRate), "0013")
			return nil, sRet
		}

		if oMsg.BetData.Username != user.Username {
			beego.Error(user, "Username Error", oMsg.BetData)
			//{code:0004, description:Username Error }
			sRet := models.NewReturnMsg(nil, models.MSG_PRI_BET_OK, "Username Error", "0004")
			return nil, sRet
		}

		sufficient, err := this.CheckAccount(user.Wid, oMsg.BetData.Amount)
		if err != "" {
			unsubscribe <- oMsg.SessionId
			return nil, err
		} else {
			if !sufficient {
				return nil, models.NewReturnMsg(nil, models.MSG_PRI_BET_OK, INSUFFICIENT_FUND, "0005")
			}
		}
	case models.MSG_USER_CANCEL:
	case models.MSG_USER_CASH_OUT:
	case models.MSG_USER_JOIN:
		/////////////////////////////////////////////
	default:
		fmt.Println("ERROR User MSG")
		return nil, ERROR_ORDER_PARAMS
	}
	return oMsg, SUCCESS
}

// broadcastWebSocket broadcasts messages to WebSocket users.
func broadcastWebSocket(event models.Event) {
	data := event.Content
	if len(data) < 5 {
		beego.Debug("BAD CONTENT", event)
		return
	}

	mSubscribers.Range(func(key, value interface{}) bool {
		if value == nil {
			beego.Error("empty:", key.(string))
			return true
		}
		ws := value.(Subscriber).Conn
		if ws != nil {
   		ws.SetWriteDeadline(time.Now().Add(writeWait))
			//beego.Debug("sending:", )
			if errSend:= ws.WriteMessage(websocket.TextMessage, []byte(data)); errSend !=nil {
				//User disconnected.
				//ws.Close()
				//mSubscribers.Delete(key)
				beego.Error(errSend)

				unsubscribe <- value.(Subscriber).Name
			}else {
				//beego.Debug("sent:",key.(string))
			}
		}else {
			//beego.Debug("nil value in mSub", )
			unsubscribe <- key.(string)
		}
		return true //continue range
	})
	//beego.Debug("published->", event)
}

//{"mesId":30,"sessionId":"D86672DECE6F7FAD08146A9D670467EB","data":{}}
/*

 */
func sendPrivate(event models.Event) {
	data := event.Content
	if len(data) < 5 {
		beego.Error("Bad data")
		return
	}
	oSub, _ := mSubscribers.Load(event.User)
	if oSub != nil {
		ws := oSub.(Subscriber).Conn
		if ws != nil {
			ws.SetWriteDeadline(time.Now().Add(writeWait))
			if errWrite := ws.WriteMessage(websocket.TextMessage, []byte(data)); errWrite != nil {
				beego.Warn(errWrite)
				unsubscribe <- oSub.(Subscriber).Name
			}
		}
		return
	}
	beego.Debug("Fail to connect to:", event.User)
}
