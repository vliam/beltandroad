package controllers

import (
	"errors"
	"regexp"
	"strings"
	//"beltAndRoad/models"
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/validation"

	"beltAndRoad/models"
	"beltAndRoad/service"
	"time"
	"hash/crc32"
)

// Predefined const error strings.
const (
	host            = "http://139.162.35.213:8082"
	sign            = "1t42-"
	ErrAmount       = "错误的金额"
	ErrInsufficient = "余额不足"
	ErrInputData    = "数据输fd入错误"
	ErrDatabase     = "数据库操作错误"
	ErrDupUser      = "用户信息已存在"
	ErrNoUser       = "用户信息不存在"
	ErrPass         = "密码不正确"
	ErrNoUserPass   = "用户信息不存在或密码不正确"
	ErrNoUserChange = "用户信息不存在或数据未改变"
	ErrInvalidUser  = "用户信息不正确"
	ErrOpenFile     = "打开文件出错"
	ErrWriteFile    = "写文件出错"
	ErrSystem       = "操作系统错误"
	QueNumLimit = 3
)

// Predefined controller error values.
var (
	err404          = &models.ControllerError{404, 404, "page not found", "page not found", ""}
	errInputData    = &models.ControllerError{400, 10001, "数据输入错误", "客户端参数错误", ""}
	errDatabase     = &models.ControllerError{500, 10002, "服务器错误", "数据库操作错误", ""}
	errDupUser      = &models.ControllerError{400, 10003, "用户信息已存在", "数据库记录重复", ""}
	errNoUser       = &models.ControllerError{400, 10004, "用户信息不存在", "数据库记录不存在", ""}
	errPass         = &models.ControllerError{400, 10005, "用户信息不存在或密码不正确", "密码不正确", ""}
	errNoUserPass   = &models.ControllerError{400, 10006, "用户信息不存在或密码不正确", "数据库记录不存在或密码不正确", ""}
	errNoUserChange = &models.ControllerError{400, 10007, "用户信息不存在或数据未改变", "数据库记录不存在或数据未改变", ""}
	errInvalidUser  = &models.ControllerError{400, 10008, "用户信息不正确", "Session信息不正确", ""}
	errOpenFile     = &models.ControllerError{500, 10009, "服务器错误", "打开文件出错", ""}
	errWriteFile    = &models.ControllerError{500, 10010, "服务器错误", "写文件出错", ""}
	errSystem       = &models.ControllerError{500, 10011, "服务器错误", "操作系统错误", ""}
	errExpired      = &models.ControllerError{400, 10012, "登录已过期", "验证token过期", ""}
	errPermission   = &models.ControllerError{400, 10013, "没有权限", "没有操作权限", ""}
	errBadUa        = &models.ControllerError{400, 10014, "登陆环境改变", "重新登陆", ""}
)

const (
	ERROR_ORDER_EXIST  = "ERR_ORDER_EXIST"
	ERROR_NO_ORDER     = "ERR_NO_ORDER"
	ERROR_GAME_ID      = "ERR_GAME_ID"
	ERROR_ORDER_PARAMS = "ERR_ORDER_PARAMS"
	ERR_SESSION        = "ERR_SESSION"
	SUCCESS            = "SUCCESS"
	INSUFFICIENT_FUND  = "INSUFFICIENT_FUND"
)

// BaseController definiton.
type BaseController struct {
	beego.Controller
	userSess models.UserSession
}

// cross domain
func (base *BaseController) Prepare() {

	base.Ctx.Output.Header("Access-Control-Allow-Credentials", "true")
	base.Ctx.Output.Header("Access-Control-Allow-Headers", "Content-Type, *")
	base.Ctx.Output.Header("Access-Control-Allow-Methods", "POST")
	base.Ctx.Output.Header("Access-Control-Allow-Origin", "http://localhost:63342")
	base.Ctx.Output.Header("Access-Control-Max-Age", "3600")

}

// RetError return error information in JSON.
func (base *BaseController) RetError(e *models.ControllerError) {
	if mode := beego.AppConfig.String("runmode"); mode == "prod" {
		e.DevInfo = ""
	}

	base.Ctx.Output.Header("Content-Type", "application/json; charset=utf-8")
	base.Ctx.ResponseWriter.WriteHeader(e.Status)
	base.Data["json"] = e
	base.ServeJSON()

	base.StopRun()
}

var sqlOp = map[string]string{
	"eq": "=",
	"ne": "<>",
	"gt": ">",
	"ge": ">=",
	"lt": "<",
	"le": "<=",
}

// ParseQueryParm parse query parameters.
//   query=col1:op1:val1,col2:op2:val2,...
//   op: one of eq, ne, gt, ge, lt, le
func (base *BaseController) ParseQueryParm() (v map[string]string, o map[string]string, err error) {
	var nameRule = regexp.MustCompile("^[a-zA-Z0-9_]+$")
	queryVal := make(map[string]string)
	queryOp := make(map[string]string)

	query := base.GetString("query")
	if query == "" {
		return queryVal, queryOp, nil
	}

	for _, cond := range strings.Split(query, ",") {
		kov := strings.Split(cond, ":")
		if len(kov) != 3 {
			return queryVal, queryOp, errors.New("Query format != k:o:v")
		}

		var key string
		var value string
		var operator string
		if !nameRule.MatchString(kov[0]) {
			return queryVal, queryOp, errors.New("Query key format is wrong")
		}
		key = kov[0]
		if op, ok := sqlOp[kov[1]]; ok {
			operator = op
		} else {
			return queryVal, queryOp, errors.New("Query operator is wrong")
		}
		value = strings.Replace(kov[2], "'", "\\'", -1)

		queryVal[key] = value
		queryOp[key] = operator
	}

	return queryVal, queryOp, nil
}

// ParseOrderParm parse order parameters.
//   order=col1:asc|desc,col2:asc|esc,...
func (base *BaseController) ParseOrderParm() (o map[string]string, err error) {
	var nameRule = regexp.MustCompile("^[a-zA-Z0-9_]+$")
	order := make(map[string]string)

	v := base.GetString("order")
	if v == "" {
		return order, nil
	}

	for _, cond := range strings.Split(v, ",") {
		kv := strings.Split(cond, ":")
		if len(kv) != 2 {
			return order, errors.New("Order format != k:v")
		}
		if !nameRule.MatchString(kv[0]) {
			return order, errors.New("Order key format is wrong")
		}
		if kv[1] != "asc" && kv[1] != "desc" {
			return order, errors.New("Order val isn't asc/desc")
		}

		order[kv[0]] = kv[1]
	}

	return order, nil
}

// ParseLimitParm parse limit parameter.
//   limit=n
func (base *BaseController) ParseLimitParm() (l int64, err error) {
	if v, err := base.GetInt64("limit"); err != nil {
		return 10, err
	} else if v > 0 {
		return v, nil
	} else {
		return 10, nil
	}
}

// ParseOffsetParm parse offset parameter.
//   offset=n
func (base *BaseController) ParseOffsetParm() (o int64, err error) {
	if v, err := base.GetInt64("offset"); err != nil {
		return 0, err
	} else if v > 0 {
		return v, nil
	} else {
		return 0, nil
	}
}

// VerifyForm use validation to verify input parameters.
func (base *BaseController) VerifyForm(obj interface{}) (err error) {
	valid := validation.Validation{}
	ok, err := valid.Valid(obj)
	if err != nil {
		return err
	}
	if !ok {
		str := ""
		for _, err := range valid.Errors {
			str += err.Key + ":" + err.Message + ";"
		}
		return errors.New(str)
	}

	return nil
}

//在这里检查session id
//检查request中的sid，然后返回用户信息
func (base *BaseController) CheckSession(bRefresh bool) (u *models.UserSession, e *models.ControllerError) {
	//1. exist 2. expired ? -> refresh
	//if good, userSession.refreshTime = new
	sid := base.Ctx.Input.Cookie("sid")

	if len(sid) < 3 {
		sid = base.GetString("sid")
	}

	if len(sid) < 3 {
		beego.Error("invalid sid")
		base.Redirect("/", 302)
	}

	if n, ok :=mInQueMsgNumber.Load(sid); ok {
		if n.(uint)> QueNumLimit{
			beego.Error(sid, "req too many times")
			base.Redirect("/", 302)
		}
	}
	//kv := strings.Split(authString, " ")
	//sessionId是不是已有
	//如果没有去memcached 那拿；拿到后放进内存，设定时间
	//如果时间达到10分钟，就RefreshSession去请求接口，看是不是还有效；如果有效就更新时间
	//sTimeRequest := base.GetString("t")
	//u =mymemcache.GetSession(sid)
	sess, _ := beego.GlobalSessions.GetSessionStore("bomb")
	s := sess.Get(sid)
	if s != nil {
		base.userSess = s.(models.UserSession)
		//		beego.Info(time.Since(base.userSess.RefreshTime).Seconds())
	}

	if bRefresh || s == nil || time.Since(base.userSess.RefreshTime).Seconds() > 300 {
		u := new (models.UserSession)
		err := new(models.ControllerError)
		iCsc := crc32.ChecksumIEEE([]byte(sid))% 1000
		for {//各个用户，都会单独地运行这个for
				if _,ok := service.LockBusySid.Load(iCsc); ok{
					time.Sleep(100* time.Millisecond)
					beego.Warn("http thread busy: " , iCsc)
				}else {
					break
				}
		}
		u, err = service.RefreshSessionHttp(sid, iCsc)
		if u != nil {
			sess.Set(u.Sid, *u)
			if err != nil {
				beego.Error(err)
			}
			base.userSess =*u
			return u, nil
		} else {
			return nil, err
		}
	} else {
		return &base.userSess, nil
	}
	return nil, errExpired
}

//sessionId是不是已有
//如果没有去memcached 那拿；拿到后放进内存，设定时间
//如果时间达到10分钟，就RefreshSession去请求接口，看是不是还有效；如果有效就更新时间
//sTimeRequest := base.GetString("t")
//	u =mymemcache.GetSession(sid)
/*if u!= nil {
	return u, nil
}
return nil, errExpired
}*/

func (base *BaseController) CheckAccount(wid string, dAmountNeed float64) (bool, string) {
	if act, err := models.GetAccount(wid, base.userSess.UserId); err != nil {
		return false, ERR_SESSION
	} else {
		return act.Amount >= dAmountNeed, ""
	}
}
