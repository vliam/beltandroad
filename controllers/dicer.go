package controllers

import (
)
import (
	"beltAndRoad/models"
	"strconv"
	"sync"
	"time"
	"math/rand"
	"github.com/astaxie/beego"
	"sort"

)

/**
 * 流程： 比赛开始时（started时刻投注已定），calOdds传入一包数据，数据为： 当前全站累计profit，当前全站累计投注额 ，rtp
 * 要得到多少％利润 在这一局里，各个玩家的订单 init version Dec 11 2017： 在calOdds时，去prepare列表里面的user
 * 先不实现cashOut动态变更odds next version：
 * 一旦有人投注，就调用prepareUser(username)；这样加快准备（无视取消订） 实现cashOut ，手动取现时调用这个方法更新odds
 *
 * 2018-01-11 增加了累计奖金池的设定
 * logic: 当累计奖金池达到dNeedPoolSize时，
 * 0124：返奖最多开到奖池那么多，即潜在的损失不能超过奖池
 */
var (
	mOddsAmount = make(map[float64]float64)
	mOddsEV = make(map[float64]float64)
	mOddsHistoryAmount = make(map[float64]float64)
)
type Dicer struct {
	MaxAmountLimit float64
	RTP float64
	dPool float64
	dSafeOdds float64
	dTotalAmt float64
	crashPoss float64
	nPoolSizeForNextOpen int
	c float64
	models.GameStatus
	bInReturnMode bool
	nOdds int

	maxRate float64
	minWinMultiLimit float64
	maxWinMultiLimit float64
	iNatureThousandth int
	iCurrentThousandth int
	poolThres float64
	///////////////////////////////////////
	mUserEV map[string]float64
	mUserTotalBet map[string]float64
	mUserTotalBonus map[string]float64
	///////////////////////////////////////
	aHappiness []float64
	aOddsList []float64
	aAmt []float64
	aEV []float64
	aWinMulti []float64
	aWinRTP []float64
	aProfitOfLetWin []float64
	mOddsIndex map[float64]int
	//////////////////////////////////////////
	PoolSize int
	//private static Map<String, Double> mapUnderEV = new HashMap<String, Double>();
}
func (d *Dicer) LoadConfig(){
	//////////////////////////////////////////////////////////////////////////////
	d.RTP, _ = beego.AppConfig.Float("bomb::RTP")
	if d.RTP > 99 ||d.RTP <70{
		d.RTP = 90
	}
	d.RTP = (d.RTP  ) /100

	d.minWinMultiLimit, _ =  beego.AppConfig.Float("bomb::minWinMultiLimit")
	d.maxWinMultiLimit, _ =  beego.AppConfig.Float("bomb::maxWinMultiLimit")

	d.poolThres, _ = beego.AppConfig.Float("bomb::poolThres")

	beego.Info("rtp, minWinLimit, maxWinlimit, poolThres:",
		d.RTP, d.minWinMultiLimit, d.maxWinMultiLimit, d.poolThres)
}
func (d *Dicer) Init(){
	d.mUserEV = make(map[string]float64)
	d.mUserTotalBet = make(map[string]float64)
	d.mUserTotalBonus = make(map[string]float64)
	d.bInReturnMode = true
	/////////////////////////////////////////////////////////////////////
	d.MaxAmountLimit, _ = beego.AppConfig.Float("bomb::maxAmount")
	d.maxRate, _ = beego.AppConfig.Float("bomb::maxRate")
	d.iNatureThousandth , _ = beego.AppConfig.Int("bomb::iNatureThousandth")
	d.iCurrentThousandth = d.iNatureThousandth
}
func (d *Dicer) UserCashOut(username string , amount float64){
	if ev, ok:= d.mUserEV[username];ok {
		d.mUserEV[username] = ev + amount
	}else {
		beego.Error("error mUserEV")
	}
	if bonus, ok:= d.mUserTotalBonus[username];ok {
		d.mUserTotalBonus[username] = bonus + amount
	}else {
		d.mUserTotalBonus[username] = amount
	}
}
func (d *Dicer) InitRound(gameId uint64, mOrder *sync.Map){
	d.LoadConfig()
	beego.Warn("Loaded RTP:", d.RTP,
		" WinMultiLimit(min, max):", d.minWinMultiLimit, d.maxWinMultiLimit)

	beego.Info("userEV:",d.mUserEV)
	beego.Info("userBT:",d.mUserTotalBet)
	beego.Info("userBN:",d.mUserTotalBonus)
	//get the order info
	//here is the lis
	length:=0
	mOrder.Range(func(_, _ interface{}) bool {
		length++
		return true
	})
	d.crashPoss = 0
	if length == 0 {
		d.crashPoss = 0.45// by default,,
		return
	}
	///////////////////////////////////////////////////////////////
	beego.Debug("--->", gameId)
	d.iCurrentThousandth = d.iNatureThousandth
	//整理订单
	//order[1.01] = [....]
	mOddsAmount = make(map[float64]float64)
	mOddsEV = make(map[float64]float64)
	d.aOddsList = make([]float64,0)

	d.dTotalAmt = 0
	//for _, currentOrder := range *mOrder{
	mOrder.Range(func(key, value interface{}) bool {
		currentOrder:= value.(*models.Order)
		odds , err:= strconv.ParseFloat(currentOrder.Number , 64)
		if err != nil {
			beego.Debug(err)
			return false
		}

		if ev, ok:= d.mUserEV[currentOrder.Username];ok {
			d.mUserEV[currentOrder.Username] = ev - currentOrder.Amount * d.RTP
		}else {
			//the user first time, get his EV from database
			d.mUserEV[currentOrder.Username] = - currentOrder.Amount * d.RTP
		}

		if amt, ok:= d.mUserTotalBet[currentOrder.Username];ok {
			d.mUserTotalBet[currentOrder.Username] = amt +currentOrder.Amount
		}else {
			d.mUserTotalBet[currentOrder.Username] = currentOrder.Amount
		}

		//user's total amt in this odds
		if dAmt, ok:= mOddsHistoryAmount[odds];ok {
			mOddsHistoryAmount[odds] = dAmt + d.mUserTotalBet[currentOrder.Username]
		}else {
			mOddsHistoryAmount[odds] = d.mUserTotalBet[currentOrder.Username]
		}

		//sum EV of this odds
		if dEVofOdds, ok:= mOddsEV[odds];ok {
			mOddsEV[odds] = dEVofOdds + d.mUserEV[currentOrder.Username]
		}else {
			mOddsEV[odds] = d.mUserEV[currentOrder.Username]
		}
		//sum bet amt of this odds
		if dAmt, ok:= mOddsAmount[odds];ok {
			mOddsAmount[odds] = dAmt + currentOrder.Amount
		}else {
			d.aOddsList = append(d.aOddsList, odds)
			mOddsAmount[odds] = currentOrder.Amount
		}
		d.dTotalAmt += currentOrder.Amount
		return true
	})

	beego.Debug("totalAmt:",d.dTotalAmt)
	models.AddValue("bet", d.dTotalAmt)
	dBet:=	models.GetValue("bet")
	dBonus:=	models.GetValue("bonus")
	d.dPool = dBet * d.RTP  - dBonus
	beego.Debug(gameId, "Starting: ",dBet, dBonus)
	beego.Warn("Pool:",dBet , " - " , dBonus,"=>",  d.dPool)
	d.dSafeOdds = d.dPool / d.dTotalAmt - 0.01
	if d.dSafeOdds < 1 {
		d.dSafeOdds = (100.0 + float64(rand.Intn(10)))/100
	}

	sort.Float64s(d.aOddsList)
	beego.Warn("odds list :", d.aOddsList)
	//////////////////////////////////////////////////////
	d.aAmt = d.aAmt[:0]
	d.aEV = d.aEV[:0]
	d.aHappiness = d.aHappiness[:0]
	d.aWinMulti = d.aWinMulti[:0]
	d.aProfitOfLetWin = d.aProfitOfLetWin[:0]
	///////////////////////////////////////////////////////
	d.nOdds = len(d.aOddsList)
	stepProfit := d.dTotalAmt //意思是说分别让第i个人赢的本局收益
	maxHappiness:= -999999.99
	minHappiness := 999999.99
	minWinMulti:= 999999.99
	maxWinMulti:=  -999999.99
	//maxWinRTP:= 0
	for i:=0 ; i < len(d.aOddsList); i++ {
		//get the total amount, total EV of odds
		d.aAmt = append(d.aAmt, mOddsAmount[d.aOddsList[i]])
		d.aEV = append(d.aEV, mOddsEV[d.aOddsList[i]])

		///////////////////////////////if pot is big ///////////////////////////////
		//mOddsHistoryAmount
		dTmpWinMulti := mOddsEV[d.aOddsList[i]]/ d.MaxAmountLimit
		// 比如当前投注11,那么限制就在 11* maxMultiLimit * 2
		//比如当前投注500，那么限制就在maxMultiLimit*500,两者取大值



		if dTmpWinMulti < mOddsEV[d.aOddsList[i]] / d.aAmt[i] /2 {
			dTmpWinMulti = mOddsEV[d.aOddsList[i]] / d.aAmt[i] /2
		}
		d.aWinMulti = append(
			d.aWinMulti,
			dTmpWinMulti)
		if minWinMulti >  d.aWinMulti[i]{
			minWinMulti =  d.aWinMulti[i]
		}
		if maxWinMulti <  d.aWinMulti[i]{
			maxWinMulti =  d.aWinMulti[i]
		}
		//////////////////////////////////////////////////////////////////////////
		// if win how much to win
		dHappiness := d.aEV[i] / (d.aAmt[i] * (d.aOddsList[i] -1))
		if dHappiness < minHappiness{
			minHappiness = dHappiness
		}
		if dHappiness > maxHappiness {
			maxHappiness = dHappiness
		}

		if dHappiness > 0{
			beego.Warn("happiness>0: ",dHappiness, d.aEV[i] , d.aAmt[i] )
			if   dHappiness < 10{//用户只要happiness未超过10先不罚
				dHappiness = 0
			}
		}
		d.aHappiness =  append(d.aHappiness, dHappiness)

		stepProfit -= d.aAmt[i] * (d.aOddsList[i])
		d.aProfitOfLetWin = append(d.aProfitOfLetWin , stepProfit)// if let guy i wins
	}
	beego.Debug("amt:",d.aAmt)
	beego.Warn("EV:",d.aEV)
	beego.Debug("Win Multi:",d.aWinMulti)
	beego.Warn("happiness:",d.aHappiness)
	beego.Debug("ProfitOfLetWin:",d.aProfitOfLetWin)
	//todo calc the order info
	d.GameId = gameId

	if percent(1) {
		d.setRate(100)
		d.crashPoss = 0
		beego.Warn( "set to 1.00, RTP:", d.RTP)
		return
	}
	//////////////////////////
	i:=0
	for ; i < len(d.aProfitOfLetWin); i++{
		if d.aProfitOfLetWin[i] < 0 {
			break
		}
	}// i - 1 is the last one can win
	iLetWin := i
	if i >0 {
		iLetWin = rand.Intn(i)
	}else {
		iLetWin = -1
	}
	///////////////////////////////
	bProfitThisGame := true //profit mode
	dPoolSize :=-1*  d.dPool / d.aProfitOfLetWin[ len(d.aOddsList) -1 ]
	beego.Warn("pool size:" , dPoolSize, " MinWinMulti:", minWinMulti,
		" MaxWinMulti:", maxWinMulti)
	if d.bInReturnMode && dPoolSize < 1 {// turn off return mode
		d.bInReturnMode = false
		d.nPoolSizeForNextOpen = rand.Intn(10 ) +1
		beego.Warn("turn off, next open when size > ", d.nPoolSizeForNextOpen )
	}

	if d.bInReturnMode{
		if dPoolSize >= d.poolThres && percent(70) {
			bProfitThisGame = false
		}else{
			if percent(60){
				// in return mode 60% chance that we need to profit this game
					bProfitThisGame = false
				}
			}
	}else{
		if dPoolSize > float64(d.nPoolSizeForNextOpen){
			beego.Warn("***turn ON")
			d.bInReturnMode = true
		}
		if percent(40){
			bProfitThisGame = false
		}
	}

	beego.Warn("profit iLetWin:", iLetWin)

	overMinMulti := minWinMulti - d.minWinMultiLimit
	overMaxMulti := maxWinMulti - d.maxWinMultiLimit
	overMulti := overMaxMulti
	if overMulti < overMinMulti {
		overMulti = overMinMulti
	}
	if overMulti > 0&& percent(40){//用户已+EV too much
		bProfitThisGame = true
	}
	if !bProfitThisGame{
		beego.Warn("not profit this round")
		//if maxHappiness < -5 {
		if dPoolSize >= d.poolThres {
			if (maxHappiness > 10 && percent(60)) || percent(40){ //40% -> only clear who's hapiness<0
				iLetWin = 0
				for ; iLetWin <  d.nOdds ; iLetWin ++{
					if d.aHappiness[iLetWin] >= 0{
						break
					}
				}
				if iLetWin>= 0 {
					iLetWin = iLetWin -1
				}
			}else{
				iLetWin = d.nOdds -1
			}

			beego.Warn("clear the whole team :", iLetWin )
		}else{
			iClearHalf := -1
			for i:= 0; i< d.nOdds -1 ; i ++{
				maxHappinessFirstHalf := -999.99
				for j:=0 ; j <= i ; j ++{
					if maxHappinessFirstHalf < d.aHappiness[j]{
						maxHappinessFirstHalf = d.aHappiness[j]
					}
				}
				minHappiness2ndHalf := 99.99
				for k:=i+1 ; k< d.nOdds ; k ++{
					if minHappiness2ndHalf > d.aHappiness[k]{
						minHappiness2ndHalf = d.aHappiness[k]
					}
				}
				if maxHappinessFirstHalf < minHappiness2ndHalf -1 {
					iClearHalf = i
					beego.Warn("half clear at:", i)
					break
				}
			}
			tmpLetWin := d.calcM()
			if tmpLetWin == -1 || d.aProfitOfLetWin[tmpLetWin] + d.dPool > 0{
				//d.GameRate = int(d.calcCrashRateByIndex(iLetWin) * 100)
				iLetWin = tmpLetWin
				beego.Warn("good Let win:", "(", iLetWin,")")
			}
			if iLetWin < iClearHalf {
				iLetWin = iClearHalf
			}
		}
	}else {//will profit
		if iLetWin >= 0 {
			i:= 0
			for  ; i < iLetWin ; i++ {
				if d.aHappiness[iLetWin] >=0 {
					break
				}
			}
			iLetWin = i-1 // when profit , do not let happy guy profit
		}
	}
	beego.Warn("iLetWin to calc:",iLetWin)
	d.GameRate = int(d.calcCrashRateByIndex(iLetWin) * 100)
	beego.Warn("gameRate:", d.GameRate, "(", iLetWin,"), safe:", d.dSafeOdds)
	/*if overMulti > 0 {//用户已+EV too much
		d.GameRate = reCalcRate(overMulti , d.GameRate)
		beego.Warn("punish, new:", d.GameRate, "; overMulti: ", overMulti)
	} else */
	if maxHappiness >0 {
		d.iCurrentThousandth = 6
	}
	if overMulti >= 0 && percent(90){ //90%
		ceiling :=int((d.minWinMultiLimit - overMulti) *100)
		if ceiling > d.GameRate || ceiling < 100{
			ceiling = d.GameRate
		}
		if ceiling < 400 {
			ceiling = 400
		}
		dCrashOdds := 1.0
		//使用随机数保证用户不管在哪里取都是亏的
		if percent(10){
			if overMulti > 10 {
				dCrashOdds = 10
			}else {
				dCrashOdds = overMulti
			}
		}else {
			dCrashOdds = 2
		}

		d.GameRate = reCalcRate(dCrashOdds, ceiling	)
		beego.Warn("overMulti >= 0:",minWinMulti, "; ", "reCalc:" , dCrashOdds, "new:", d.GameRate)
	}
}
func percent (r int )bool {
	if 	rand.Intn(100) < r {
		return true
	} else {
		return  false
	}
}
func thousandth (r int )bool {
	if 	rand.Intn(1000) < r {
		return true
	} else {
		return  false
	}
}
func reCalcRate( crashOdds float64, ceiling int)  int{
	iBaseOdds := 1
	for i:= 100; i<= ceiling; i++{
		if rand.Intn(200) < int(crashOdds + 0.5) + iBaseOdds{
			return i
		}
	}
	return ceiling
}
func (d *Dicer) calcM()int{
	//处罚第一个逆站队的朋友
	//消去：消去的时候还要保证站队是好的
	for i:= 0 ; i < len(d.aHappiness); i++{
		/*if d.aHappiness[i] > -1{
			return i - 1
		}*/
		bInLine := true
		for j:=i+1 ; j < len(d.aHappiness); j++{
			if d.aHappiness[i] >d.aHappiness[j]{
				bInLine = false
				break
			}
		}
		if !bInLine {
			return i - 1
		}
	}
	return len(d.aHappiness) -1// all good

}
//总是去消去照顾EV最大的，成排消去,使得里面的happiness > 外面
// 里面的人happiness增长的快
//happiness的意思是说，这家伙要赢多少次才回到RTP
// = EV / (amount * (odds -1))

func (d *Dicer) setRate(finalRate int){
	d.GameRate = finalRate
}
func (d *Dicer) GetRate()float64{
	//get the order info
	return float64(d.GameRate) /100
}
func (d *Dicer) CheckRate(GameRate int) int{
	if GameRate >= int(d.maxRate + 0.5) *100 { //hits the limit
		return GameRate
	}
	if d.crashPoss != 0 {// empty order mode
		rand.Seed(time.Now().UnixNano())
		if rand.Float64()*100 <= d.crashPoss {
			return GameRate
		} else {
			return 0
		}
	}
	if GameRate >= d.GameRate {
		return d.GameRate
	}else{
		if thousandth(d.iCurrentThousandth){
			beego.Warn("Thousandth @",GameRate)
			return GameRate
		}
		return 0
	}
}
//public static List<OrderInfo> getOrderList(String issue) {
//public static boolean calOdds(String issue) {
//private static void calUserEV(String username, String db) {


func (d *Dicer) calcCrashRateByIndex(i int )float64 {
	i+= 1
	var dAdjustment float64
	dAdjustment = 0.0
	res := 1.0
	if i>=1 && i == d.nOdds  {// 所有订单都能达到
		delta := (d.maxRate  - d.aOddsList[d.nOdds - 1]) * 100
		rateCeiling:= d.maxRate
		if delta >  d.maxRate * 100 /2 {
			if rand.Intn(10) < 8 {
				rateCeiling /= 2
				beego.Debug("newRateCeiling:", rateCeiling," - max odds:" , d.aOddsList[d.nOdds-1] )
				delta = (rateCeiling - d.aOddsList[d.nOdds-1]) * 100
			}
		}
		if delta / 2 > 1 {
			tmpAdjust := rand.Intn(int(delta))
			dAdjustment = float64(tmpAdjust)* 0.01
		}

		res = rateCeiling - dAdjustment
	}else {
		if i > 0  {
			delta := (d.aOddsList[i] - d.aOddsList[i - 1]) * 100
			if delta / 2 > 1 {
				tmpAdjust :=  rand.Intn(int(delta))
				dAdjustment = float64(tmpAdjust)* 0.01
			}
		}else if  i == 0 && d.aOddsList[i] > 1.01 {
			delta := (d.aOddsList[i] - 1) * 100
			if delta / 2 > 1 {
				tmpAdjust := rand.Intn(int(delta))
				dAdjustment = float64(tmpAdjust) * 0.01
			}
		}
		res =d.aOddsList[i] - dAdjustment - 0.01

		if res > d.dSafeOdds {
			beego.Warn("use safe:", d.dSafeOdds)
			res = d.dSafeOdds
		}
	}

	//logger.debug("*dAdjustment: "+ dAdjustment);
		//resultOdds := d.aOddsList[i] - dAdjustment - 0.01

	return res
}