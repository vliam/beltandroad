package controllers

import (
	"sync"
	"time"

	"beltAndRoad/models"

	"github.com/astaxie/beego"
	"github.com/gorilla/websocket"
	"strings"
)

type Subscription struct {
	Archive []models.Event      // All the events from the archive.
	New     <-chan models.Event // New events coming in.
}

func newEvent(ep models.EventType, user, msg string) models.Event {
	return models.Event{ep, user, int(time.Now().Unix()), msg}
}

func Join(user string, ws *websocket.Conn, fullName string) {
	beego.Info( fullName, " try to join: ",user)
	subscribe <- Subscriber{Name: strings.TrimSpace(user), Conn: ws, FullName: strings.TrimSpace(fullName)}
}

func Leave(user string) {
	unsubscribe <- user
}

type Subscriber struct {
	Name string
	FullName string
	Conn *websocket.Conn // Only for WebSocket users; otherwise nil.
}

var (
	// Channel for new join users.
	subscribe = make(chan Subscriber, 65535)
	// Channel for exit users.
	unsubscribe = make(chan string, 65535)
	// Send events here to publish them.
	publish    = make(chan models.Event, 65535)
	privateQue = make(chan models.Event, 65535)
	cmdQue     = make(chan models.MsgCmd, 65535)
	mInQueMsgNumber  = new(sync.Map)
	//subscribers = list.New()
	mSubscribers = new(sync.Map)
)

// This function handles all incoming chan messages.
func ticashRoom() {
	for {
		select {
		case unsub := <-unsubscribe:
			oSub, ok := mSubscribers.Load(unsub)
			if ok {
				beego.Debug("to Close:"  , unsub)
				if oSub != nil {
					ws := oSub.(Subscriber).Conn
					if ws != nil {
						err := ws.Close()
						if err != nil {
							beego.Error(err)
						} else {
							beego.Debug("WebSocket closed:", unsub)
						}
					}
				}
				mSubscribers.Delete(unsub)
			}

		case event := <-publish:
		//	beego.Debug("publish->", event)
			broadcastWebSocket(event)

		case event := <-privateQue:
			beego.Debug("private->", event)
			sendPrivate(event)
		}

	}
	beego.Emergency("!!!ticker que down!!!")
}
func joinHandler() {
	for {
		select {
		case sub := <-subscribe:
			beego.Info("JOIN->", sub)
			if !isUserExist(sub.Name) {
				mSubscribers.Store(sub.Name, sub)
				if v, ok := mUserSessionId.Load(sub.FullName); ok {
					sid := v.(string)
					if sid != sub.Name {
						unsubscribe <- sid
						//beego.Warn( sub.FullName,"old:", sid , "new:" ,sub.Name)
					}
				}
				mUserSessionId.Store(sub.FullName, sub.Name)
				////unsubscribe <- oMsg.SessionId
				// Publish a JOIN event.
				//publish <- newEvent(models.EVENT_JOIN, sub.Name, "")
				//beego.Warn("New user:", sub.Name)
			} else {
				//beego.Debug("Old user:", sub.Name)
			}
		}
	}
	beego.Emergency("!!!join que down!!!")
}
func cmdHandler() {
	var iValue uint
	sMsg := ""
	for {
		select {
		case cmd := <-cmdQue:
			n, ok := mInQueMsgNumber.Load(cmd.SessionId)
			if ok {
				mInQueMsgNumber.Store(cmd.SessionId, n.(uint)-1)
			} else {
				iValue = 0
				mInQueMsgNumber.Store(cmd.SessionId, iValue)
			}
			beego.Debug("CMD->", cmd)
			if cmd.MesId == models.MSG_USER_CANCEL {
				sMsg = cancelOrder(cmd)
			} else if cmd.MesId == models.MSG_USER_BET {
				sMsg = newOrder(cmd)
			} else if cmd.MesId == models.MSG_USER_CASH_OUT {
				sMsg = cashOutOrder(cmd.Username, cmd.BetData.CashOut, 0)
			} else if cmd.MesId == models.MSG_USER_JOIN {
				sMsg = snapShot(cmd.Username)
			}
			if len(sMsg) > 1 { //response
				if cmd.MesId == models.MSG_USER_CASH_OUT &&
					strings.Contains(sMsg, "stopped_at") { //cash_out_ok
					publish <- newEvent(models.EVENT_MESSAGE, cmd.SessionId, sMsg)
				} else {
					privateQue <- newEvent(models.EVENT_MESSAGE, cmd.SessionId, sMsg)
				}
			} else {
				beego.Debug(cmd, "sMsg null:", sMsg)
			}
		}
	}
	beego.Emergency("!!!cmd que down!!!")
}
func init() {
	if beego.BConfig.RunMode == "test" {
		return
	}
	beego.Info("Yo init ticash Room")
	go ticashRoom()
	go InsertOrUpdateOrderQue()
	//go AccountOrderQue()
	go CallBackQue()
	go cmdHandler()
	go joinHandler()
}
func isUserExist(sessionId string) bool {
	//beego.Warn(sessionId)
	//beego.Warn(mSubscribers)
	if _, ok := mSubscribers.Load(sessionId); ok {
		return true
	}
	return false
}
