package controllers

import (
	"beltAndRoad/models"
	"fmt"
	"github.com/astaxie/beego"
	"math/rand"
	"strconv"
	"strings"
	"sync"
)

func init() {
	// init models.Issue{}
	// test Issue function
	// Need to register model in init
	if beego.BConfig.RunMode != "test" {
		return
	}
	fmt.Println("init test ctrl")
	//TestDicer()
}
func TestDicer(){
	//make mOrders
	aUserName := [20]string{"test1", "test2", "test3", "test4","test5","test6","test7","test8","test9","test10",
		"test11", "test12", "test13", "test14","test15","test16","test17","test18","test19","test20"	}

	aBonus := [20]float64{0,0,0,0,0,   0,0,0,0,0,    0,0,0,0,0,   0,0,0,0,0}
	aBetAmt := [20]float64{0,0,0,0,0,   0,0,0,0,0,    0,0,0,0,0,   0,0,0,0,0}
	aRTP := [20]float64{0,0,0,0,0,   0,0,0,0,0,    0,0,0,0,0,   0,0,0,0,0}
	mOrder := new(sync.Map)
	nPlayer := 10
	models.ClearValue("bonus")
	models.ClearValue("bet")


	d := Dicer{}
	d.Init()
	nMaxPlayer := nPlayer
	for gameId:=0; gameId <10000; gameId ++ {
		for i:=0 ; i < nPlayer ; i++ {
			if gameId > 1000 {// 0 - 2000
				nMaxPlayer = 1
				//continue
			}
			if gameId > 2000{
				nMaxPlayer = 10
			}

			v := aUserName[i]
			iOdds := rand.Intn(1000) + 100
			dOdds  := float64(iOdds)/100
			sOdds :=fmt.Sprint(dOdds)
			orderA := models.NewOrder("test", "test",
				001, v,
				"test", 1, sOdds, 10		)
			//mOrder[orderA.Username] = orderA
			mOrder.Store(orderA.Username, orderA)
			if i >= nMaxPlayer {
				mOrder.Delete(orderA.Username)
			}
		}

		d.InitRound(uint64(gameId), mOrder)
		//settle the orders
		//for k,v := range mOrder{
		mOrder.Range(func(key, value interface{}) bool {
			v:= value.(*models.Order)
			odds, err:=  strconv.ParseFloat(v.Number, 64)
			index:=	getNumber(v.Username)

			if err == nil {
				if odds <= d.GetRate(){
					dBonus := v.Amount * odds

					d.UserCashOut(key.(string), dBonus)
					models.AddValue("bonus", dBonus)

					//beego.Warn(gameId, "bonus:",dBonus)
					if index >= 0{
						aBonus[index -1 ] += dBonus
					}else{
						beego.Error("ERROR user index")
					}
				}
			}
			aBetAmt[index -1 ] += v.Amount
			aRTP[index -1 ] = aBonus[index -1 ] / aBetAmt[index -1 ]
			return true
		})
		beego.Warn(gameId, "RTP:",aRTP)
	}
	d.InitRound(1, mOrder)
	fmt.Println(mOrder)
}
func getNumber(s string) int {
	sNew :=strings.Replace(s, "test", "",1)
	i, err := strconv.Atoi(sNew)
	if err != nil{
		return -1
	}
	return i
}