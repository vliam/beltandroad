package controllers

import (
	"beltAndRoad/models"
	"sync"

	"encoding/json"
	"fmt"
	"strconv"

	"github.com/astaxie/beego"
	//"hash/fnv"
	"container/list"
	"time"
	"beltAndRoad/service"
	"strings"
	"crypto/md5"
)

//type OrderMap map[string]*models.Order
//type OrderMap sync.Map

var (
	mOrderPool  = new(sync.Map)
	mMeron     = new(sync.Map)
	mMeronNext = new(sync.Map)
	mUserSessionId  = new(sync.Map)
	mCanceledOrder  = new(sync.Map)
	isBusy   = new(sync.Map)
	isWaiting = new(sync.Map)

	orderQue    = make(chan *models.Order, 4095)
	actOrderQue = make(chan *models.Order, 4095)
	extraList  = list.New()
	//valueNewOrder interface{}
	valueTickOrder interface{}
	mapGameCrash interface{}

)

//when game is open ,the orders will cache here
/*
 */
func newOrder(o models.MsgCmd) string {
	//if wala, create  v this map
	ok:=false
	var v interface{}
	v,ok = mOrderPool.Load(oGame.OpenGameId)//valueNewOrder
	if ok {
		tmpM := v.(*sync.Map)//tmpMapNewOrder
		if _, ok := tmpM.Load(o.Username); ok {
			//		beego.Error("ERROR_ORDER_EXIST")
			return models.NewReturnMsg(nil, models.MSG_PRI_BET_OK, "ERROR_ORDER_EXIST", "4101")
		}
	}
	//userid, username, lotteryCode, issue,t, amount
	tmpOrder := models.NewOrder(o.SessionId, o.Wid,
		o.Userid, o.Username,
		o.BetData.LotteryCode,
		oGame.OpenGameId,
		fmt.Sprintf("%.2f", o.BetData.Rate), o.BetData.Amount)
	//////////////////////////////////
	act, err := models.GetAccount(o.Wid, o.Userid)
	if err == nil {
		if act.Amount < tmpOrder.Amount {
			beego.Error("not enough fund")
			return models.NewReturnMsg(nil, models.MSG_PRI_BET_OK, "ERROR_BALANCE", "4102")
		}
	} else {
		beego.Error("ERROR get act:", err)
		return fmt.Sprint(err)
	}

	orderQue <- tmpOrder
	mMeronNext.Store(int(o.BetData.Rate*100 + 0.5 ), true)//+0.5坑
	////////////////////////////////////
	return ""
}/*
func addGameOrderToQue(iGameId uint64) {
	if iGameId != oGame.GameId || oGame.GameId != oGame.OpenGameId {
		beego.Error("status error", oGame)
		return
	}
	value, _ := mOrderPool.Load(iGameId)
	tmpMap= value.(*sync.Map)
	tmpMap.Range(func(key, v interface{}) bool {
		orderQue <- v.(*models.Order)
		return true
	})
	tmpMap = nil

}*/

//tick odds <->
func cancelOrder(o models.MsgCmd) string {
	//if meron order
	value, _ := mOrderPool.Load(oGame.OpenGameId)
	msg := models.NewReturnMsg(nil, models.MSG_PRI_CANCEL_ERR, "No Order", "")
	if value != nil {
		currentMap := value.(*sync.Map)
		v, ok := currentMap.Load(o.Username)
		if ok {
			//todo: check if there is already canceled order
			n, _ := models.MeronCancelOrder(o.Wid, oGame.OpenGameId,  o.Userid)
			if n ==0 {
				v.(*models.Order).Status = models.ORDER_STATUS_CANCEL
				mCanceledOrder.Store(v.(*models.Order).OrderNo, true)
				orderQue <- v.(*models.Order)
				v = nil
				return ""
			}else{
				msg = models.NewReturnMsg(nil, models.MSG_PRI_CANCEL_ERR, "Can only cancel once", "")
			}
		}
		currentMap = nil
		value = nil
	}
	return msg
}
func CancelOrderCallBack(order *models.Order) {
	mCanceledOrder.Delete(order.OrderNo)

	sMsg := models.NewReturnMsg(nil, models.MSG_PRI_CANCEL_OK, "Canceled", "")
	privateQue <- newEvent(models.EVENT_MESSAGE, order.Sid, sMsg)
	value, _ := mOrderPool.Load(order.Issue)
	tmpMapCancel := value.(*sync.Map)
	tmpMapCancel.Delete(order.Username)
	mOrderPool.Store(order.Issue,tmpMapCancel)

}

func PlaceOrderCallBack(order *models.Order) {
	beego.Info("newOrder:", order)
	sMsg := models.NewReturnMsg(order, models.MSG_PRI_BET_OK, fmt.Sprint(order.IndexInThisGame), "")
	privateQue <- newEvent(models.EVENT_MESSAGE, order.Sid, sMsg)
}
func getOrderArray() ([]models.Order, float64, float64) {
	aOrder := make([]models.Order, 0)
	dMaxAmount := 0.0
	dTotalAmount := 0.0
	vTmp, _ := mOrderPool.Load(oGame.GameId)

	if vTmp != nil {
		m := vTmp.(*sync.Map)
		//for _, v := range m.(interface{}).(OrderMap){
		m.Range(func(key, value interface{}) bool {
			v := value.(*models.Order)
			if len(v.Username) > 1 {
				aOrder = append(aOrder, *v)
				aOrder[len(aOrder)-1].Number = ""
				aOrder[len(aOrder)-1].Issue = 0

				aOrder[len(aOrder)-1].Username = service.Mosaic(aOrder[len(aOrder)-1].Username )
				aOrder[len(aOrder)-1].Hash = fmt.Sprintf("%x", md5.Sum([]byte(strings.ToLower(v.Username +"_" + v.Wid))))

				dTotalAmount += v.Amount
				if v.Amount > dMaxAmount {
					dMaxAmount = v.Amount
				}
			}
			return true
		})
	}
	return aOrder, dMaxAmount, dTotalAmount
}
func snapShot(username string) string {
	//oGame.GameId
	//oGame.GameStatus
	oMsg := models.MsgWelcome{MesId: models.MSG_PRI_WELCOME}
	oMsg.Data.Issue = oGame.GameId
	oMsg.Data.Status = oGame.GameStatus // starting , started
	oMsg.Data.Bets, _, _ = getOrderArray()
	oMsg.Data.HaveBet = ""

	value, _ := mOrderPool.Load(oGame.OpenGameId)
	if value != nil {
		tmpMapSnap:= value.(*sync.Map)
		if _, ok := tmpMapSnap.Load(username); ok {
			oMsg.Data.HaveBet = fmt.Sprint(oGame.OpenGameId)
		} else if v, ok := tmpMapSnap.Load(username); ok {
			if v.(*models.Order).Status == models.ORDER_STATUS_INIT {
				oMsg.Data.CanCashOut = fmt.Sprint(oGame.GameId)
			} else {
				oMsg.Data.CanCashOut = ""
			}
		}
	}
	sRet, _ := json.Marshal(oMsg)
	return string(sRet)
}

func gameCrash(rate float64) {
	mapGameCrash, _ = mOrderPool.Load(oGame.GameId)
	maxBet := 0.0
	totalBet := 0.0
	nOrders :=0
	if mapGameCrash != nil {
		tmpMapCrash := mapGameCrash.(*sync.Map)
		//for _, v := range m.(interface{}).(OrderMap){
		tmpMapCrash.Range(func(key, value interface{}) bool {
			v := value.(*models.Order)
			if v.Status != models.ORDER_STATUS_CANCEL{
				dNumber, _ := strconv.ParseFloat(v.Number, 64)
				totalBet += v.Amount
				nOrders ++
				if v.Amount > maxBet {
					maxBet = v.Amount
				}
				if v.Status == 0{
					if dNumber > rate {
						v.Status = models.ORDER_STATUS_MISS
						orderQue <- v
					}else {
						beego.Error("ERR: remaining open",v)
					}
				}
			}
			return true
		})
	}
	///////////////--extra bonus--////////////////////////////
	var next *list.Element
	totalExtraBonus :=	float64(int((totalBet /100 )*100))/100
	if nOrders == 1 {
		totalExtraBonus =0
	}
	beego.Warn("totalExtra:",totalExtraBonus, " maxBet:", maxBet)
	remainBonus := totalExtraBonus
	for v := extraList.Front(); v!= nil;v = next{
		next = v.Next()
		order := v.Value.(*models.Order)
		if remainBonus >0 {
			bonusForMe := order.Amount/maxBet * totalExtraBonus
			if remainBonus < bonusForMe{
				bonusForMe = remainBonus
			}
			order.ExtraBonus = float64(int((bonusForMe)*100))/100

			orderQue <- order
			beego.Warn("amt:",order.Amount, "get: ", bonusForMe, order)
			remainBonus -= bonusForMe
		}
		extraList.Remove(v)
	}

	beego.Info("after game, added all orders")
	//loss weight
	mOrderPool.Range(func(key, value interface{}) bool {
		num := key.(uint64)
		if num > 0 && oGame.GameId > num && oGame.GameId-num > 20 {
			beego.Warn(oGame.GameId, "delete:", num, oGame.GameId-num)
			mOrderPool.Delete(num)
		}
		return true
	})
	/*if _, ok := mOrderPool.Load(oGame.GameId - 100); ok{
		mOrderPool.Delete( oGame.GameId -100)
	}*/
}

// input: odds
//close the orders
func tickOrder(rate float64) {
	//send stop_at msg
	valueTickOrder, _ = mOrderPool.Load(oGame.GameId)
	if valueTickOrder != nil {
		tmpMapTick := valueTickOrder.(*sync.Map)
		//for _, v := range currentMap.(interface{}).(OrderMap){
		tmpMapTick.Range(func(key, value interface{}) bool {
			v := value.(*models.Order)
			dNumber, _ := strconv.ParseFloat(v.Number, 64)
			if v.Issue != oGame.GameId{
				beego.Error(oGame.GameId, "Order in Error map:",v)
				return true//continue to next element
			}
			if v.Status == 0 && dNumber <= rate {
				sRet := cashOutOrder(v.Username, dNumber, oGame.GameId )
				beego.Info("hit cash out:", v.Username, dNumber)
				publish <- newEvent(models.EVENT_MESSAGE, "", sRet)
			}
			return true
		})
	}
}

//this is when player gets his bonus
func cashOutOrder(username string, rate float64, gameId uint64 ) string {
	iRate := int(rate * 100)
	if gameId ==0 {//manual cash out
		gameId = oGame.GameId
	}
	//if meron order
	value, _ := mOrderPool.Load(gameId)
	if value != nil {
		tmpMapCashOut := value.(*sync.Map)
		vTmp, ok := tmpMapCashOut.Load(username)
		if ok {
			v := vTmp.(*models.Order)
			if v.Issue != oGame.GameId{
				beego.Error( oGame.GameId,"orderInWrongMap:", v)
				return ""
			}
			if oGame.GameId != oGame.OpenGameId {
				if v.Status == models.ORDER_STATUS_INIT {
					if rate <= 1 { //err
						sRet := models.NewReturnMsg(nil, models.MSG_USER_CASH_OUT_RETURN, "ERROR_STOP_AT", "4231")
						return sRet
					}
					if iRate <= oGame.GameRate {
						if _, ok = mCanceledOrder.Load(v.OrderNo); ok{
							sRet:= models.NewReturnMsg(nil, models.MSG_USER_CASH_OUT_RETURN, "ERROR_STOP_AT", "4231")
							beego.Error("try to cashout a canceled order")
							return sRet
						}
						v.Status = models.ORDER_STATUS_WIN
						v.Odds = rate

						v.BonusAmount, _ = strconv.ParseFloat(fmt.Sprintf("%.2f",
							rate * v.Amount), 64)

						models.AddValue("bonus", v.BonusAmount)

						dicer.UserCashOut(v.Username, v.BonusAmount)

						extraList.PushFront(v)
						if extraList.Len() >3 {
							it := extraList.Back()
							extraList.Remove(it)
						}

						beego.Info("cash Out :",v)
						orderQue <- v
						sHash := fmt.Sprintf("%x", md5.Sum([]byte(strings.ToLower(v.Username +"_" + v.Wid))))
						//v.Wid
						return models.NewStopMsg(v.Amount, service.Mosaic(v.Username), v.Odds, sHash)

					} else {
						sRet := models.NewReturnMsg(nil, models.MSG_USER_CASH_OUT_RETURN, "ERR_RATE", "4232")
						return sRet
					}
				} else {
					sRet := models.NewReturnMsg(nil, models.MSG_USER_CASH_OUT_RETURN, "ERR_STATUS", "4233")
					return sRet
				}
			}
		} else {//still open
			beego.Error("NO Order:", oGame.GameId, username)
		}
	}
	return models.NewReturnMsg(nil, models.MSG_USER_CASH_OUT_RETURN, "ERR_NO_ORDER", "4234")
}
func InsertOrUpdateOrderQue() {
	//when this game closed, clear the map[issue - 10]
	//write the current orders into db
	//after close
	for {
		select {
		case o := <-orderQue:
			beego.Info("orderQue>>>", o)

			if o.Status == models.ORDER_STATUS_INIT && o.OrderNo == "" {
				//new order, deduct
				//make a order number
				//insert order -> account detail -> account
				//if ok --> add amount to cache
				//if error not modify and log
				act, _ := models.GetAccount(o.Wid, o.Userid)
				if act.Amount < o.Amount {
					beego.Error("not enough fund")
					continue //skip this order
				}
			}
			ok:=false
			var v interface{}
			v,ok = mOrderPool.Load(o.Issue)//valueNewOrder
			if !ok {
				v = new(sync.Map)
				mOrderPool.Store(o.Issue, v)
				beego.Warn("make orderMap", o.Issue)
			}
			fullName := o.Wid+"_"+ o.Username
			for{
				if _,ok:= isWaiting.Load(fullName); !ok{
					break
				}else {
					beego.Error(fullName, "isWaiting, block.")
					time.Sleep(100*time.Millisecond)
				}
			}
			go func() {
				for {
					if _, ok = isBusy.Load(fullName); ok{ //user lock
						beego.Error(fullName, "Update busy, wait for 100 ms")
						isWaiting.Store(fullName, true)
						time.Sleep(100*time.Millisecond)
					}else {
						isBusy.Store(fullName, true)
						break
					}
				}
				isWaiting.Delete(fullName)

				err := o.AddOrUpdate()
				if err != nil {
					beego.Error(o,"ERROR PLACE:", err)
					if !strings.Contains(err.Error(), "Duplicate"){
						if o.Id != 0 && o.Status == 2{
							beego.Warn("put back: ", o)
							orderQue <- o
						}
					}
				} else {
					models.CallBackQue <- o
					v,ok = mOrderPool.Load(o.Issue)//valueNewOrder
					if !ok{
						beego.Error(o,"ERROR NO orderMap:", o )
					}
					tmpMapUpdate := v.(*sync.Map)//tmpMapNewOrder
					tmpMapUpdate.Store(o.Username, o)
					mOrderPool.Store(o.Issue, tmpMapUpdate)
					beego.Info("update finish:", o)
				}
				isBusy.Delete(fullName)
			}()
			/*//update order status and amount
				//if error not modify and log
				//update order -> account detail -> account
				if o.ProcessFlag == 1 && o.ExtraBonus != 0 {
					//write extraBonus
			}*/

		}
	}
}
func CallBackQue() {
	for {
		select {
		case o := <-models.CallBackQue:
			if o.Status == models.ORDER_STATUS_CANCEL {
				//beego.Warn("CANCEL call back >>", o)
				CancelOrderCallBack(o)
			}else if o.Status == models.ORDER_STATUS_INIT {
				//beego.Warn("PLACE call back >>", o)
				PlaceOrderCallBack(o)
			}else if o.Status == models.ORDER_STATUS_WIN {
				//beego.Warn("WIN call back >>", o)
			}else if o.Status == models.ORDER_STATUS_MISS {
				//beego.Warn("MISSED call back >>", o)
			}
		}
	}
}
