package controllers

import (
	//	"container/list"
	"beltAndRoad/models"
	"beltAndRoad/service"
	"sync"
	"time"
	//"github.com/astaxie/beego"
	//"github.com/gorilla/websocket"
	//"github.com/beego/samples/WebIM/models"
	//"fmt"

	"fmt"
	"strconv"

	"github.com/astaxie/beego"
	"math/rand"
)

var (
	//ticker       *time.Ticker
	oGame        models.GameStatus
	dicer        Dicer
	interval_1   int
	interval_2   int
	interval_3   int
	timeStarting int
	SpeedTh      int
	SpeedTh_2 int
	mGameResult  = make(map[uint64]float64)
	mIssue       *models.Issue
	mNextIssue   *models.Issue
	lotteryCode  string
)

func changeStatus() {
}
func fixLastIssue() {
	lastIssue := models.GetLastIssue(beego.AppConfig.String("bomb::lotteryCode"), models.STATUS_STARTED)
	if lastIssue.Status == models.STATUS_STARTING || lastIssue.Status == models.STATUS_STARTED {
		//cancel them
	}
}

// get issue info from data base
func initIssue() {
	mIssue = models.GetLastIssue(lotteryCode, models.STATUS_INIT)
	if mIssue == nil {
		mIssue = models.GetLastIssue(lotteryCode, models.STATUS_OPEN)
	}
	if mIssue == nil {
		mIssue = models.CreateIssue(lotteryCode)
	}

	mIssue.Status = models.STATUS_OPEN
	for {
		_, err := mIssue.Update()
		if err != nil {
			beego.Error("err update issue:",err)
			time.Sleep(1000)
		}else{
			break
		}
	}

	mNextIssue = models.CreateIssue(lotteryCode)

	beego.Warn("new issue:", mIssue)
	oGame.GameId, _ = strconv.ParseUint(mIssue.Issue, 10, 64)
	oGame.OpenGameId, _ = strconv.ParseUint(mIssue.Issue, 10, 64)
	//currentGameStatus
	//order list
	oGame.GameRate = 100
	oGame.GameStatus = models.STATUS_OPEN
	oGame.TimeStarted = time.Now()
}
func gameStarting(gameId uint64) {
	oGame.GameStatus = models.STATUS_STARTING
	mIssue.Status = models.STATUS_STARTING
	for {
		_, err := mIssue.Update()
		if err != nil {
			beego.Error("err update issue:",err)
			time.Sleep(1000)
		}else{
			break
		}
	}
	//addGameOrderToQue(oGame.OpenGameId)
	beego.Info("gameStarting:", oGame)
}
func gameStarted(gameId uint64) {
	oGame.TimeStarted = time.Now()
	oGame.GameStatus = models.STATUS_STARTED
	mIssue.Status = models.STATUS_STARTED
	for {
		_, err := mIssue.Update()
		if err != nil {
			beego.Error("err update issue:",err)
			time.Sleep(1000)
		}else{
			break
		}
	}

	for {
		mNextIssue = models.CreateIssue(lotteryCode)
		if mNextIssue != nil {
			break
		}
		beego.Error("err create issue, retry")
		time.Sleep(1000)
	}

	oGame.OpenGameId, _ = strconv.ParseUint(mNextIssue.Issue, 10, 64)

	mMeron = mMeronNext

	mMeronNext = new(sync.Map)
	beego.Debug("mMeron:", mMeron)
	beego.Debug("mMeronNext:", mMeronNext)
	//todo: write the new issue into db
	currentMap , _ :=  mOrderPool.Load(oGame.GameId)
	if currentMap != nil {
		dicer.InitRound(oGame.GameId, currentMap.(*sync.Map))
	}else {
		dicer.InitRound(oGame.GameId, new(sync.Map))
	}
	beego.Warn("gameStarted:", oGame)
}
func gameStop(gameId uint64) {
	dGameRate := float64(oGame.GameRate) / 100
	mIssue.BonusNumber = fmt.Sprintf("%.2f", dGameRate)
	gameCrash(dGameRate)
	mIssue.Status = models.STATUS_END
	for {
		_, err := mIssue.Update()
		if err != nil {
			beego.Error("err update issue:",err)
			time.Sleep(1000)
		}else{
			break
		}
	}

	mIssue = mNextIssue
	mIssue.Status = models.STATUS_OPEN
	for {
		_, err := mIssue.Update()
		if err != nil {
			beego.Error("err update issue:",err)
			time.Sleep(1000)
		}else{
			break
		}
	}

	oGame.GameId, _ = strconv.ParseUint(mIssue.Issue, 10, 64)
	oGame.GameStatus = models.STATUS_OPEN
	oGame.GameRate = 100
}

func tick() {
	sMsg := ""
	//ticker = time.NewTicker(time.Duration(interval_1) * time.Millisecond)
	iIntervalNow := interval_1
	step := 1

	for {
		select {
		case <- time.After( time.Duration(iIntervalNow)  * time.Millisecond)://ticker.C:
			if oGame.GameRate > SpeedTh_2 {
				iRange := oGame.GameRate/100
				if iRange > 15 {
					iRange = 15
				}
				step = rand.Intn(iRange) +1
			}else{
				step =1
			}
			tEsp := int(time.Since(oGame.TimeStarted).Seconds() * 1000)
			//tick then send tick event to ws

			if oGame.GameStatus == models.STATUS_STARTED {
				iTickRate := oGame.GameRate +1
				oGame.GameRate += step
				var finalTick int
				beego.Debug("gameRate:", oGame.GameRate )
				for ; iTickRate<= oGame.GameRate;iTickRate++{
					finalTick = dicer.CheckRate(iTickRate)
					if finalTick <=0 {
						if _, ok := mMeron.Load(iTickRate); ok {
							beego.Info("meron tick rate:", iTickRate)
							tickOrder(float64(iTickRate) / 100)
						}
					}else{
						tickOrder(float64(finalTick) / 100)
						oGame.GameRate = finalTick
						break
					}
				}

				if oGame.GameRate > SpeedTh {
					//ticker = time.NewTicker(time.Duration(interval_2) * time.Millisecond)
					iIntervalNow = interval_2
				}


				if finalTick > 0 {
					//game stop
					//ticker = time.NewTicker(3000 * time.Millisecond)
					iIntervalNow = 3000
					//mGameResult[oGame.GameId] = float64(finalTick) / 100
					iStep:= 10000
					iPool := int(models.GetValue("bet") - models.GetValue("bonus") )

					if iPool < iStep {
						iPool += iStep
					}else {
						iMulti :=  int (iPool /iStep)
						if iMulti > 10 {
							iMulti = 10
						}
						iPool = iMulti * iStep + (iPool - iMulti * iStep) / iMulti
					}
					sMsg = models.NewCrashMsg(tEsp, float64(finalTick)/100, make([]models.Order, 1), iPool)
					publish <- newEvent(models.EVENT_MESSAGE, "", sMsg)
					beego.Warn("game stop:", oGame.GameId, "@", finalTick)

					gameStop(oGame.GameId)
					continue
				} else {
					sMsg = models.NewTickMsg(tEsp, oGame.GameRate)
				}
			} else if oGame.GameStatus == models.STATUS_OPEN {
				//ticker = time.NewTicker(time.Duration(timeStarting) * time.Millisecond)
				iIntervalNow = timeStarting
				gameStarting(oGame.GameId)
				sMsg = models.NewStartingMsg(oGame.GameId, getMaxWin())

			} else if oGame.GameStatus == models.STATUS_STARTING {
				//ticker = time.NewTicker(time.Duration(interval_1) * time.Millisecond)
				iIntervalNow = interval_1
				gameStarted(oGame.GameId)
				aOrder, maxAmt ,_:= getOrderArray()
				beego.Warn("orderCount:", len(aOrder))
				beego.Debug(aOrder)
				sMsg = models.NewStarted(oGame.GameId, maxAmt, aOrder)
			}
			publish <- newEvent(models.EVENT_MESSAGE, "", sMsg)
		}
	}
	beego.Error("clock Stopped", iIntervalNow)

}

func getMaxWin() float64 {
	return 10000
}
func init() {
	service.InitServ()
	if beego.BConfig.RunMode == "test" {
		beego.Error("***TEST Mode, not init the game")
		return
	}
	interval_1, _ = beego.AppConfig.Int("bomb::Interval_1")
	interval_2, _ = beego.AppConfig.Int("bomb::Interval_2")
	interval_3, _ = beego.AppConfig.Int("bomb::Interval_3")

	timeStarting, _ = beego.AppConfig.Int("bomb::timeStarting")
	SpeedTh, _ = beego.AppConfig.Int("bomb::SpeedThreshold")
	SpeedTh_2, _ = beego.AppConfig.Int("bomb::SpeedThreshold_2")
	lotteryCode = beego.AppConfig.String("bomb::lotteryCode")

	initIssue()

	dicer = Dicer{}
	dicer.Init()
	dicer.InitRound(oGame.GameId, new(sync.Map))

	go tick()
}
