package main

import (
	"beltAndRoad/controllers"

	_ "beltAndRoad/routers"

	"github.com/astaxie/beego"
	"github.com/astaxie/beego/logs"
	//"fmt"
	"github.com/astaxie/beego/session"
	"fmt"
	"math/rand"
)

var log *logs.BeeLogger
var globalSessions *session.Manager

func main() {
	//////////////////////////////
	sessionconf := &session.ManagerConfig{
		CookieName: "begoosessionID",
		Gclifetime: 3600,
	}

	beego.GlobalSessions, _ = session.NewManager("memory", sessionconf)
	go beego.GlobalSessions.GC()

	////////////////////////////
	//	cache.Register("file",cache.NewFileCache) // this operation is run in init method of file.go.

	/*
			log := logs.NewLogger(10000)  // 创建一个日志记录器，参数为缓冲区的大小
		        log.SetLogger("console", "")  // 设置日志记录方式：控制台记录
		        log.SetLevel(logs.LevelDebug) // 设置日志写入缓冲区的等级：Debug级别（最低级别，所以所有log都会输入到缓冲区）
			log.EnableFuncCallDepth(true) // 输出log时能显示输出文件名和行号（非必须）*/

	if beego.BConfig.RunMode == "dev" {
		beego.BConfig.WebConfig.DirectoryIndex = true
		beego.BConfig.WebConfig.StaticDir["/swaggerz"] = "swagger"
	}
	beego.SetLogger("file", `{"filename":"./gameServer.log"}`)
	iLevel, _ := beego.AppConfig.Int("logLevel")
	beego.SetLevel(iLevel)

	//0720 12:23 fix: when duplicate, do not put the order back
	beego.Warn("release 0720 12:23")


	/*str := `WcO▒&cO▒11cO▒UcO▒K#630684D6418364C7E6DCEB5A45F3D9CF-n1{"userinfo":{"agentid":0,"agentname":"","class":"com.sby.bean.Userinfo","createtime":1523945418000,"dbid":"t2","forwardOpen":0,"id":53844,"info":"","locktime":null,"loginCount":239761,"loginIp":"139.162.23.175","merchantId":null,"password":"46cc468df60c961d8da2326337c7aa58","paypasswd":null,"rebate_info":"0.0|0.0|1","status":1,"token":"1978606b-8512-484e-aca5-66d341e2fe6c","type":1,"updatetime":1526049085279,"upuNames":"admin","username":"AI19","wid":"t2","withdrawCount":5},"account":{"amount":102497.2,"bonusAmount":217157.2,"class":"com.sby.bean.Account","createtime":1523945478000,"freezeAmount":0.0,"id":53074,"presentAmount":0.0,"rechargeAmount":0.0,"score":0,"status":0,"updatetime":1526037566000,"userid":53844,"username":"AI19"}}`
	sRes := mymemcache.GetJSONFromCache(str)
	u := new(models.UserInfo)
	_ = json.Unmarshal([]byte(sRes), u)
	fmt.Println(u)
	*/
	fmt.Println(float64(rand.Intn(100))/100)
	beego.Run()
	if beego.BConfig.RunMode == "test" {
		controllers.TestDicer()
	}
}
