/*
0529 版本：调用process order接口
*/
package service

import (
	"beltAndRoad/models"
	"beltAndRoad/models/mymemcache"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"
	"time"

	"encoding/json"
	"net"

	"github.com/astaxie/beego"
)

var (
	err          error
	aResp        []*http.Response
	aClient      []*http.Client
	resp         *http.Response
	client       *http.Client
	errInputData = &models.ControllerError{400, 10001, "数据输入错误", "客户端参数错误", ""}
	orderQue     [100]chan *models.Order
)

func InitProcesser() {
	var netTransport = &http.Transport{
		Dial: (&net.Dialer{
			Timeout: 5 * time.Second,
		}).Dial,
		DisableKeepAlives:   true,
		TLSHandshakeTimeout: 5 * time.Second,
	}
	/*var netClient = &http.Client{
		Timeout: time.Second * 10,
		Transport: netTransport,
	}*/
	for i := 0; i < 100; i++ {
		aClient = append(aClient, &http.Client{
			Timeout:   time.Second * 10,
			Transport: netTransport,
		})
		aResp = append(aResp, &http.Response{})
		orderQue[i] = make(chan *models.Order, 4095)
		go goProcessOrder(i)
	}
	client = &http.Client{
		Timeout:   time.Second * 10,
		Transport: netTransport,
	}
	resp = &http.Response{}
}
func QueOrder(o *models.Order, index int) {
	orderQue[index] <- o
}
func goProcessOrder(index int) {
	fmt.Println("init processer:", index)
	for {
		select {
		case o := <-orderQue[index]:
			if o.Status == models.ORDER_STATUS_MISS {
				continue
			}
			v := url.Values{"ordernumber": {o.OrderNo},
				"status": {fmt.Sprint(o.Status)},
				"wid":    {o.Wid}}
			s := v.Encode()

			url := beego.AppConfig.String("accountServer") +
				"/bomb/account/processOrder.do" +
				";jsessionid=" + o.Sid + "?" +
				"ordernumber=" + fmt.Sprint(o.OrderNo) +
				"&status=" + fmt.Sprint(o.Status) +
				"&wid=" + fmt.Sprint(o.Wid)
			beego.Warn(url)
			req, reqErr := http.NewRequest("GET", url, strings.NewReader(s))
			if reqErr != nil {
				beego.Error(reqErr)
				orderQue[index] <- o
				continue
			}
			//req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
			req.Header.Set("Connection", "close")

			aResp[index], err = aClient[index].Do(req)
			if err != nil {
				beego.Error("err request :", err)
				orderQue[index] <- o
				continue
			}
			defer aResp[index].Body.Close()
			body, err := ioutil.ReadAll(aResp[index].Body)
			if err != nil {
				beego.Error(err)
				orderQue[index] <- o
				continue

			}
			aResp[index].Body.Close()
			aResp[index].Close = true
			beego.Warn(index, "process finished:", string(body))
			models.CallBackQue <- o
		}
	}
}
func ProcessOrder(o *models.Order, index int) error {
	//http://139.162.23.175:8083/bomb/info/checkSession.do;jsessionid=AA42664853B1D18625C26B811F4B6878
	if o.Status == models.ORDER_STATUS_MISS {
		return nil
	}
	v := url.Values{"ordernumber": {o.OrderNo},
		"status": {fmt.Sprint(o.Status)},
		"wid":    {o.Wid}}
	s := v.Encode()

	url := beego.AppConfig.String("accountServer") +
		"/bomb/account/processOrder.do" +
		";jsessionid=" + o.Sid + "?" +
		"ordernumber=" + fmt.Sprint(o.OrderNo) +
		"&status=" + fmt.Sprint(o.Status) +
		"&wid=" + fmt.Sprint(o.Wid)
	beego.Warn(url)
	req, reqErr := http.NewRequest("GET", url, strings.NewReader(s))
	if reqErr != nil {
		return reqErr
	}
	//req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Set("Connection", "close")

	aResp[index], err = aClient[index].Do(req)
	if err != nil {
		beego.Error("err request :", err)
		return err
	}
	defer aResp[index].Body.Close()
	body, err := ioutil.ReadAll(aResp[index].Body)
	if err != nil {
		//panic(err) // panic seems harsh, usually you'd just return the error. But either way...
		beego.Error(err)
		return err
	}

	aResp[index].Body.Close()
	if err != nil {
		return err
	}
	aResp[index].Close = true
	beego.Warn(index, "process finished:", string(body))
	return nil
}

//在这里检查session id
//检查request中的sid，然后返回用户名
func RefreshSession(sid string) (u *models.UserSession, e *models.ControllerError) {
	//get session from memc
	//////////////////////////////
	sSession := mymemcache.GetUserInfo(sid)
	if len(sSession) < 5 {
		beego.Error("bad session memcached : ", sid)
		return nil, errInputData
	}
	var err error
	info := new(models.UserInfo)

	err = json.Unmarshal([]byte(sSession), info)
	if err != nil {
		beego.Error(err, sSession)
		return nil, errInputData
	}
	if info.UserId > 0 {
		u = new(models.UserSession)
		u.RefreshTime = time.Now().UTC()
		u.Sid = sid
		u.Wid = info.Wid
		u.UserId = info.UserId
		u.Username = info.Username
		return u, nil
	} else {
		return nil, errInputData
	}
}
