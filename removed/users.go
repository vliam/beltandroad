package models

import (
	"crypto/md5"
	"fmt"
	"time"

	"encoding/hex"

	"github.com/astaxie/beego/orm"
	_ "github.com/go-sql-driver/mysql"
)

// User model definiton.
//user的结构
type User struct {
	Id          uint64     `json:"id,omitempty"`
	Username    string    `json:"username,omitempty"`
	Password    string    `json:"password,omitempty"`
	Agentid      uint64    `json:"agentid,omitempty"`
	Createtime  time.Time `json:"createTime,omitempty"`
	Updatetime  time.Time `json:"createTime,omitempty"`
	Status      int       `json:"status,omitempty"`
	Type    int       `json:"type,omitempty"`  //1 普通；2代理；9:房主；
}

func (u *User) TableName() string {
	return "userinfo"
}
// NewUser alloc and initialize a User.
func NewUser(f *RegisterForm, t time.Time) *User {
	user := User{
		Username:   f.Username,
		Password:   f.Password,
		Createtime: t}
	return &user
}

//hash pass
func (r *User) HashPass(pass string) string {
	hasher := md5.New()
	hasher.Write([]byte(pass + r.MakeSalt()))
	return hex.EncodeToString(hasher.Sum(nil))
}

//make salt
func (r *User) MakeSalt() string {
	return r.Username[2:3] + r.Password[0:1] + r.Password[2:3]
}
// CheckPass compare input password.
func (u *User) CheckPass(pass string) bool {
	sPassword := u.HashPass(pass)
	var users []User
	n, err := orm.NewOrm().QueryTable("userinfo").Filter("username", u.Username).Filter("password", sPassword).All(&users)
	fmt.Println(n, err, len(users))
	if n == 1 {
		return true
	} else {
		return false
	}
}

// Insert insert a user recode to database.
func (r *User) Insert(o orm.Ormer) (code int, err error) {
	num, err := o.Insert(r)
	return int(num), err
}

// FindByID query a recode according to input id.
func (r *User) FindByID(id int64) (code int, err error) {
	return 0, nil
}

// ClearPass clear password information.
func (r *User) ClearPass() {
	r.Password = ""
}

// GetAllUsers query all matched records.
func GetAllUsers(queryVal map[string]string, queryOp map[string]string, order map[string]string, limit int64, offset int64) (records []User, err error) {
	return nil, nil
}

// UpdateByID update a recode accroding to input id.
func (r *User) UpdateByID(id int64, f *UpdateForm) (code int, err error) {
	return ErrNotFound, nil
}

// DeleteByID delete a record accroding to input id.
func (r *User) DeleteByID(id int64) (code int, err error) {
	return ErrNotFound, nil
}

// ChangePass update password and salt information according to input id.
func ChangePass(id, oldPass, newPass string) (code int, err error) {
	return 0, nil
}

func CheckUser(sUserName string) bool {
	//here we check if the user exists
	if len(sUserName) < 3 {
		return false
	}
	var users []User
	n, err := orm.NewOrm().QueryTable("userinfo").Filter("username", sUserName).All(&users)
	fmt.Println(n, err, len(users))
	return len(users) == 1
}
func GetUserByName(sUserName string) *User {
	var user User
	err := orm.NewOrm().QueryTable("userinfo").Filter("username", sUserName).One(&user)
	fmt.Println("getuser:",user)
	if err == nil {
		//	users[0].Password = ""
		return &user
	} else {
		return nil
	}
}

func GetUserByNameAndDB(sUserName string, wid string ) *User {
	var user User
	o:=orm.NewOrm()
	o.Using(wid)
	err :=o.QueryTable("userinfo").Filter("username", sUserName).One(&user)
	fmt.Println("getuser:",user)
	if err == nil {
		//	users[0].Password = ""
		return &user
	} else {
		return nil
	}
}
