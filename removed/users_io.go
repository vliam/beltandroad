package models

// RegisterForm definiton.
type RegisterForm struct {
	Username string `form:"username" valid:"Required;MinSize(3)"`
	NickName string `form:"nick_name"`
	Password string `form:"password" valid:"Required;MinSize(6)"`
	Mobile   string `form:"mobile"`
	Qq       string `form:"qq"`
	RegFrom  string `form:"reg_from" `
	//RegisterIp string `form:"reg_ip"`
	Agent string `form:"agent"`
}

// LoginForm definiton.
type LoginForm struct {
	Username string `form:"username"    valid:"Required"`
	Password string `form:"password" valid:"Required"`
}

type RefreshForm struct {
	Sid string `form:"sid" valid:"Required"` //这里是检查sid的
}

// LogoutForm defintion.
type LogoutForm struct {
	Wechat string `form:"wechat" valid:"Required;MinSize(3)"`
}

// PasswdForm definition.
type PasswdForm struct {
	Wechat      string `form:"wechat" valid:"Required;MinSize(3)"`
	OldPassword string `form:"old_password" valid:"Required;MinSize(6)"`
	NewPassword string `form:"new_password" valid:"Required;MinSize(6)"`
}

// UpdateForm definition.
type UpdateForm struct {
	Wechat string `form:"wechat" valid:"Required;MinSize(3)"`
	//	OldPassword string `form:"old_password" valid:"Required MinSize(6)"`
	//	NewPassword string `form:"new_password" valid:"Required MinSize(6)"`
}
