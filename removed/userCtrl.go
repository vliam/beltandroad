package controllers
/*
user controller ,my profile , my status ....
*/
import (
	"strconv"
	//	"crypto/md5"
	"beltAndRoad/models"
	"fmt"
	"time"

	"github.com/astaxie/beego"
)

// UserController definiton.
type UserController struct {
	BaseController
}

// @Title Create
// @Description create object
// @Param	sRoom		path 	string	true		"sRoom"
// @Success 200 {string} sRoom
// @Failure 403 body is empty
// @router / [post]
func (c *UserController) Register() {
	form := models.RegisterForm{}
	if err := c.ParseForm(&form); err != nil {
		beego.Debug("ParseRegsiterForm:", err)
		c.Data["json"] = models.NewErrorInfo(ErrInputData)
		c.ServeJSON()
		return
	}
	beego.Debug("ParseRegsiterForm:", &form)
	if err := c.VerifyForm(&form); err != nil {
		beego.Debug("ValidRegsiterForm:", err)
		c.Data["json"] = models.NewErrorInfo(ErrInputData)
		c.ServeJSON()
		return
	}
	/////////////after valid, check room/ agent
	regDate := time.Now()
	user := models.NewUser(&form, regDate)


	beego.Debug("NewUser:", user)
	//检验唯一性
	bUserExists := models.CheckUser(user.Username)
	if !bUserExists {
		user.Password = user.HashPass(user.Password)
		//user.Balance = 0
		if code, err := user.Insert(nil); err != nil {
			beego.Error("InsertUser:", err)
			if code == models.ErrDupRows {
				c.Data["json"] = models.NewErrorInfo(ErrDupUser)
			} else {
				c.Data["json"] = models.NewErrorInfo(ErrDatabase)
			}
			c.ServeJSON()
			return
		} else {
			c.Data["json"] = models.NewNormalInfo("注册成功！")
			c.ServeJSON()
		}
	} else {
		c.Data["json"] = models.NewErrorInfo(ErrDupUser)
		c.ServeJSON()
		return
	}
	//go models.IncTotalUserCount(regDate)
	beego.Debug(user)
}

// Login method.
// @Title Create
// @Description create object
// @Param	body		body 	models.Object	true		"The object content"
// @Success 200 {string} models.Object.Id
// @Failure 403 body is empty
// @router /login [post]
func (c *UserController) Login() {
	form := models.LoginForm{}
	if err := c.ParseForm(&form); err != nil {
		beego.Debug("ParseLoginForm:", err)
		c.Data["json"] = models.NewErrorInfo(ErrInputData)
		c.ServeJSON()
		return
	}
	beego.Debug("ParseLoginForm:", &form)

	if err := c.VerifyForm(&form); err != nil {
		beego.Debug("ValidLoginForm:", err)
		c.Data["json"] = models.NewErrorInfo(ErrInputData)
		c.ServeJSON()
		return
	}
	/////////////////////////////////
	user := models.User{}
	user.Username = form.Username
	user.Password = form.Password
	beego.Debug("UserInfo:", &user)

	if ok := user.CheckPass(form.Password); !ok {
		c.Data["json"] = models.NewErrorInfo(ErrPass)
		c.ServeJSON()
		return
	}
	user.ClearPass()
	//////////////////////////////////////////////////////////
	c.SetSession("user_id", form.Username)
	sid:=c.CruSession.SessionID()
	fmt.Println(sid)
	tNow := time.Now().Unix()

		fmt.Println(form.Username, "loginOK")

		c.Data["json"] = models.NewUserInfo(user.Username ,strconv.Itoa(int(tNow)))
		c.ServeJSON()
	}

// Refresh method.
// @Title Rfresh
// @Description refresh
// @Param	body		body 	models.Object	true		"The object content"
// @Success 200 {string}
// @Failure 403 body is empty
// @router /refresh [post]
func (c *UserController) Refresh() {
		//这里是用jwt来验证身份
		oUser, err := c.CheckSession()
		if err != nil || len(oUser.Username) < 3 {
			c.Data["json"] = err
		} else {
			if oUser != nil {
				c.Data["json"] = oUser
			} else {
				c.Data["json"] = models.NewErrorInfo("Bad username")
			}
		}
		c.ServeJSON()
}

// Account method.
// @Title Account
// @Description get account settings
// @Param	body		body 	models.Object	true		"The object content"
// @Success 200 {string}
// @Failure 403 body is empty
// @router /account [post]
func (c *UserController) Account() {
	oUser, err := c.CheckSession()
	fmt.Println(oUser, err)
	if err != nil || len(oUser.Username) < 3 {
		c.Data["json"] = err
	} else {

		if oUser != nil {
			c.Data["json"] = oUser
		} else {
			c.Data["json"] = models.NewErrorInfo("Bad username")
		}
	}
	c.ServeJSON()
}

// UpdateInfo method.
// @Title update
// @Description update user setting
// @Param	body		body 	models.Object	true		"The object content"
// @Success 200 {string}
// @Failure 403 body is empty
// @router /UpdateInfo [post]
func (c *UserController) UpdateInfo() {
	u, err := c.CheckSession()
	if err != nil || len(u.Username) < 3 {
		c.Data["json"] = err
	} else {
		u := models.GetUserByName(u.Username)
		u.Password = ""
		u.Id = 0
		if u != nil {
			c.Data["json"] = u
		} else {
			c.Data["json"] = models.NewErrorInfo("Bad username")
		}
	}
	c.ServeJSON()
}