package models

import (
	"errors"
	"fmt"
	"math/rand"
	"strconv"
	"time"

	"github.com/astaxie/beego"
	_ "github.com/go-sql-driver/mysql"
	"github.com/astaxie/beego/orm"
	"strings"
)

// Order model definiton.
//order的结构
type Order struct {
	Id       int64  `json:"-"`
	OrderNo  string `json:"-" orm:"column(order_no)"`
	Username string `json:"username,omitempty"`
	Userid   uint64 `json:"-"`
	Status   int    `json:"status,omitempty"`

	Issue       uint64  `json:"issue,omitempty"`
	LotteryCode string  `json:"lotterycode,omitempty" orm:"column(lotterycode)"`
	Amount      float64 `json:"amount,omitempty"`
	Number      string  `json:"number,omitempty"`                  // rate in the order
	Odds        float64 `json:"odds,omitempty" orm:"column(odds)"` //cashout rate
	MoneyType   int     `json:"-" orm:"column(moneyType)"`

	BonusAmount float64 `json:"bonus_amount,omitempty"`
	BonusNumber string  `json:"lotterycode,omitempty" orm:"column(bonusNumber)"`
	ExtraBonus  float64 `json:"extra_bonus,omitempty"`

	CreateTime time.Time `json:"-" orm:"column(createtime)"`
	UpdateTime time.Time `json:"-" orm:"column(updatetime)"`

	FromSource  int `json:"fromsource,omitempty" orm:"column(fromsource)"`
	ProcessFlag int `json:"-" orm:"column(process_flag)"`

	Sid      string `json:"-" orm:"-"`
	Wid      string `json:"-" orm:"-"`
	FullName string `json:"-" orm:"-"`
	IndexInThisGame int64 `json:"-" orm:"-"`

	Hash     string`json:"hash,omitempty" orm:"-"`
}

const (
	ORDER_STATUS_INIT   = 0
	ORDER_STATUS_MISS   = 1
	ORDER_STATUS_WIN    = 2
	ORDER_STATUS_CANCEL = 3
	ORDER_STATUS_EXTRA   = 9
)

// NewOrder alloc and initialize a Order.
func NewOrder(Sid string, Wid string, UserId uint64, Username string,
	LotteryCode string, Issue uint64, Number string, amount float64) *Order {
	order := Order{
		Sid:         Sid,
		Wid:         Wid,
		Username:    Username,
		LotteryCode: LotteryCode,
		Userid:      UserId,
		Issue:       Issue,
		CreateTime:  time.Now().UTC(),
		Status:      0,
		Odds:        0,
		Number:      Number,
		Amount:      amount}

	return &order
}
func (r *Order) TableName() string {
	return "order_info"
}

// FindByID query a recode according to input id.
func (r *Order) FindByID(id int64) (code int, err error) {
	return 0, nil
}

// GetAllOrders query all matched records.
func GetAllOrders(queryVal map[string]string, queryOp map[string]string, order map[string]string, limit int64, offset int64) (records []Order, err error) {
	return nil, nil
}
func MeronCancelOrder(sWid string, issue uint64, userid uint64) ( n int64 , err error) {
	o := orm.NewOrm()
	o.Using(sWid)
	n, err = o.
		QueryTable("order_info").
		Filter("issue", issue).
		Filter("userid", userid).
		Filter("status", ORDER_STATUS_CANCEL).
		Count()
		return n , err
}
// UpdateByID update a recode accroding to input id.
func (r *Order) MeronOrder(sWid string, id uint64, issue uint64) (n int64,nTotal int64,  err error) {
	o := orm.NewOrm()
	o.Using(sWid)
	nCount, err := o.
		QueryTable("order_info").
		Filter("issue", issue).
		Filter("userid", r.Userid).
		Filter("status__lt", ORDER_STATUS_CANCEL).
		Count()
	nTotal, err = o.
		QueryTable("order_info").
		Filter("issue", issue).
		Filter("userid", r.Userid).
		Count()
	return nCount,nTotal , err
}

func (r *Order) AddOrUpdate() (errRollBack error) {
	if _, ok := bOrm[r.Wid]; ok == false {
		beego.Error("bad wid", r)
		errRollBack = errors.New("bad id")
		return errRollBack
	}

	o :=  orm.NewOrm()
	o.Using(r.Wid)
	n, nTotal, _ := r.MeronOrder(r.Wid, r.Userid, r.Issue)

	if nTotal > 1 && r.Status == ORDER_STATUS_CANCEL {
		return errors.New("can only cancel once")
	}
	///////////////////////////////////
	errRollBack = o.Begin()
	//defer  o.Commit()
	if n == 0 {//no this order
		if r.Status != ORDER_STATUS_INIT {
			beego.Error("[R]try to insert but it's not INIT", r)
			errRollBack =  errors.New("try to insert but it's not INIT")
		}else{
			_, errRollBack = r.Insert()
			r.IndexInThisGame = nTotal
		}
	} else {
		if len(r.OrderNo) < 3 {
			if r.Status == 0 {
				errRollBack = errors.New("[R]error: Meron order")
			}else{
				errRollBack = errors.New("[R]error: bad orderNo when wanna update")
			}
		} else {
			beego.Debug("Update in transaction:", r)
			_, errRollBack = r.Update()
		}
	}
	if errRollBack != nil {
		beego.Error("[R]place order:", errRollBack)
	}else {//place order ok
		//when you update an order , modify the tx also
		iType := -1
		// db use innoDB then we can use tx
		if r.Status == ORDER_STATUS_INIT {
			iType = TX_TYPE_BET
		}else if r.Status == ORDER_STATUS_WIN {
			if r.ExtraBonus == 0 {
				iType = TX_TYPE_BONUS
			}else {
				iType = TX_TYPE_EXTRA
			}
		}else if r.Status == ORDER_STATUS_CANCEL {
			iType = TX_TYPE_CANCEL
		}else if r.Status == ORDER_STATUS_MISS{
			iType = 0
		}else {
			beego.Error("[R]error type", errRollBack)
		}//check r.Status
		if iType != 0 {//do nothing when 0
			if iType == TX_TYPE_EXTRA {
				if !MeronBonusTx(r.Wid, r.OrderNo){
					iType = TX_TYPE_BONUS
				}
			}
			act:= new (Account)
			act ,errRollBack= GetAccountUseOrm(o ,r.Userid)// get info from account
			if errRollBack!=nil {
				beego.Error("[R]ERR_GET_ACT", errRollBack)
			}else {//get act ok
				act, errRollBack = ChangeAct(iType, r, act )
				if errRollBack != nil {
					beego.Error("[R]Error ChangeAct:", errRollBack)
				}else{//change act value ok
					_, errRollBack = o.Update(act)
					if errRollBack != nil {
						beego.Error("[R]Error update act:", errRollBack)
					}else{
						tx := new (Tx)
						tx, errRollBack = NewOrderTx(r.Userid, iType, r, act )
						_, errRollBack = o.Insert(tx)
						if errRollBack != nil {
							beego.Error("[R]Error insert tx:" ,errRollBack)
						}
					}
				}
			}
		}//iType != 0
	}

	if errRollBack != nil{
		beego.Error("Rollback becoz:", errRollBack)
		errR := o.Rollback()
		if errR!= nil{
			beego.Error("RollbackErr:", errR)
		}
		return errRollBack
	}else{
		errRollBack = o.Commit()
	}

	return errRollBack
}

// UpdateByID update a recode accroding to input id.
func (r *Order) Insert() (n int64, err error) {
	//only one order for the bomb game
	var order Order

	o :=  orm.NewOrm()
	o.Using(r.Wid)

	err = o.QueryTable("order_info").OrderBy("-id").One(&order)
	iOrderNo, err := strconv.ParseUint(order.OrderNo, 10, 64)
	if err != nil {
		iOrderNo = 0
	}
	for {
		iOrderNo += uint64(rand.Intn(1000)+ 1 )
		r.OrderNo = fmt.Sprintf("%012d", iOrderNo)
		beego.Debug("newOrder number:", r)
		n, err = o.Insert(r)
		if err != nil {
			beego.Error("insterOrderError:",err)
			if strings.Index(err.Error(), "Duplicate") >= 0{
				beego.Error("Retry...")
				r.Id = 0
				time.Sleep(100* time.Millisecond)
			}else{
				return n, err
			}
		} else {
			r.Id = n
			break
		}
	}
	return n, nil
}

// UpdateByID update a recode accroding to input id.
func (r *Order) Update() (n int64, err error) {
	r.UpdateTime = time.Now()
	o :=  orm.NewOrm()
	o.Using(r.Wid)
	n, err = o.Update(r)
	return n, err
}
