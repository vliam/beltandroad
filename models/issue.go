package models

import (
	//	"crypto/md5"
	//	"fmt"
	"time"

	_ "github.com/go-sql-driver/mysql"
	"fmt"
	//"github.com/astaxie/beego"

//	"github.com/astaxie/beego"
	"strconv"
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
)
const (
	STATUS_INIT = 0
	STATUS_OPEN = 1
	STATUS_STARTING =2
	STATUS_STARTED =3
	STATUS_END =4
)

// User model definiton.
//connection with the database
type Issue struct {
	Id       int64   `json:"id,omitempty"`
	Issue string 	`json:"issue,omitempty"`
	Lotterycode string  `json:"lotterycode,omitempty"`
	Status int 				`json:"status,omitempty"`
	//UpdateStatus int		`json:"update_status,omitempty" orm:"column(update_status)"`
	BonusNumber string		`json:"bonus_number,omitempty"`
	CreateTime time.Time `json:"create_time,omitempty" orm:"column(createtime)"`
	UpdateTime time.Time `json:"update_time,omitempty"  orm:"column(updatetime)"`
	StartTime time.Time `json:"start_time,omitempty"  orm:"column(starttime)"`
	EndTime time.Time `json:"end_time,omitempty"  orm:"column(endtime)"`

}
func (r *Issue) TableName() string {
	return "main_issue"
}


// add a new issue into database
func (r *Issue) Insert() (n int64, err error) {
	n, err = oDefault.Insert(r)
	return n, err
}

// UpdateByID update a recode accroding to input id.
// update status of Issue & lottery code
func (r *Issue) Update() (num int64, err error) {
	//input the BonusNumber ,and status
	if r.Status == STATUS_OPEN {
		r.StartTime = time.Now()
	}else if r.Status == STATUS_END{
		r.EndTime = time.Now()
	}
	num, err = oDefault.Update(r)
	return num, err
}

/*
query the last issue from database
*/
func GetLastIssue(lotterycode string, iStatus int) *Issue {
	//get the latest issue from data base
	var issue  Issue

	o := orm.NewOrm()

	var err error
	if iStatus >=0{
		err = o.QueryTable("main_issue").Filter("lotterycode", lotterycode).Filter("status", iStatus).OrderBy("id").One(&issue)
	}else{
		err = o.QueryTable("main_issue").Filter("lotterycode", lotterycode).OrderBy("-id").One(&issue)
	}

	//check if it's today's issue
	// if issue.Issue
	//sPre := time.Now().Format("20060102")
	fmt.Println("issue:", issue)
	if err !=nil{
		beego.Info("getLastIssue:",err)
		return nil
		//issue = Issue{CreateTime:time.Now(), Status:0 , Lotterycode: lotterycode, Issue: sPre + "00001" }
	}else{
		return &issue
	}
	return nil
}

/*
query the last issue from database
create a init issue
*/
func CreateIssue(lotterycode string) *Issue {
	//get the latest issue from data base
	var issue * Issue
	issue = GetLastIssue(lotterycode, STATUS_INIT)
	if issue != nil {
		beego.Info("last init issue: " , issue.Issue)
		return issue
	}

	issue = GetLastIssue(lotterycode, -1)
	//o := orm.NewOrm()
	//err := o.QueryTable("main_issue").Filter("lotterycode", lotterycode).One(issue)
	//check if it's today's issue
	// if issue.Issue
	sPre := time.Now().Format("20060102")
	if issue == nil || issue.Issue[0:8] != sPre{
		issue = &Issue{CreateTime:time.Now(), Status: STATUS_INIT , Lotterycode: lotterycode, Issue: sPre + "0001" }
	}else {
		iNumberNow,_ := strconv.Atoi(issue.Issue[8:])
		iNumberNow ++
		beego.Info(iNumberNow)
		iIssueNow := sPre + fmt.Sprintf("%04d", iNumberNow)
		issue = &Issue{CreateTime:time.Now(), Status:0 , Lotterycode: lotterycode, Issue: iIssueNow}
	}
	n, err:=issue.Insert()
	if err != nil {
		beego.Error(n, err)
		return nil
	}else{
		return issue
	}
}