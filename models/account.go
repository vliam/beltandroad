package models

import (
	"time"

	"github.com/astaxie/beego/orm"
	_ "github.com/go-sql-driver/mysql"
	"github.com/astaxie/beego"
)

/*
1{"account":{
"amount":103542.5,"bonusAmount":218912.5,
"class":"com.sby.bean.Account",
"createtime":1523945477000,
"freezeAmount":0.0,
"id":53055,"presentAmount":0.0,
"rechargeAmount":0.0,"score":0,"status":0,
"updatetime":1526037567000,
"userid":53825,"username":"AI00"}}
*/
type Account struct {
	Id             int64
	UserId         uint64    `json:"userid" orm:"column(userid)"`
	Username       string    `json:"username"`
	BonusAmount    float64   `json:"bonusAmount"`
	RechargeAmount float64   `json:"rechargeAmount" `
	PresentAmount  float64   `json:"presentAmount"`
	FreezeAmount   float64   `json:"freezeAmount" `
	Score          int       `json:"score"`
	Amount         float64   `json:"amount"`
	Status         int64     `json:"status"`
	CreateTime     time.Time `json:"createtime" orm:"column(createtime)"`
	UpdateTime     time.Time `json:"updatetime"  orm:"column(updatetime)"`
}



func (a *Account) AddAmount(sWid string, dAmount float64) (num int64, err error) {
	a.Amount = a.Amount + dAmount
	if a.Amount < 0 {
		return -1, nil
	}
	o :=  orm.NewOrm()
	o.Using(sWid)
	num, err = o.Update(a)
	if err != nil {
		beego.Error("ERR Change Amount:",err)
	}
	return num, err
}
//FindByID query a recode according to input id.
//func (a *Account) FindByID(id int64) (code int, err error) {
//	return 0, nil
//}

func (a *Account) CheckAccountExist(o orm.Ormer, sUsername string) bool {
	exist := o.QueryTable("account").Filter("username", sUsername).Exist()
	return exist
}

func GetAccount(sWid string, userId uint64) (*Account, error) {
	var act Account

	o :=  orm.NewOrm()
	o.Using(sWid)
	err := o.QueryTable("account").Filter("userid", userId).One(&act)

	if err == nil {
		return &act, nil
	} else {
		beego.Error(err)
		return nil, err
	}
}

func GetAccountUseOrm( o orm.Ormer, userId uint64) (*Account, error) {
	var act Account
	err := o.QueryTable("account").Filter("userid", userId).One(&act)

	if err == nil {
		return &act, nil
	} else {
		beego.Error(err)
		return nil, err
	}
}
