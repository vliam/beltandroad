package models

import (
	_ "github.com/go-sql-driver/mysql"
	"time"

	"fmt"
	"errors"
	"github.com/astaxie/beego/orm"
)
/*注返点(103, "投注返点"),
开空撤单(104, "开空撤单"),
系统返奖(105, "系统返奖"),
额外加奖(106, "额外加奖"),
撤单返款(107, "撤单返款"),
投注扣款(201, "投注扣款"), 账户转出(202, "账户转出"),  账户转入(203, "账户转入"), RollBack扣款(206, "RollBack扣款"), TIE_REFUND(207, "开和退款"), 充值限额扣除(301, "充值限额扣除"), 充值限额充值(302, "充值限额充值");
 */

var (
	TX_TYPE_BET  = 201
	MEMO_BET= "投注扣款"
	TX_TYPE_BONUS = 105
	MEMO_BONUS = "系统返奖"
	TX_TYPE_EXTRA = 106
	MEMO_EXTRA = "额外加奖"
	TX_TYPE_CANCEL = 107
	MEMO_CANCEL = "撤单返款"

)
type Tx struct {
	Id int64
	Userid uint64 `json:"username"`
	Accountid float64 `json:"amount"`
	/* */
	BeforeBonusAmount float64 `json:"-"`
	AfterBonusAmount float64  `json:"-"`

	BeforeRechargeAmount float64 `json:"-"`
	AfterRechargeAmount float64  `json:"-"`

	BeforeAmount float64`json:"-" `
	AfterAmount float64  `json:"-"`

	BeforePresentAmount float64`json:"-"`
	AfterPresentAmount float64  `json:"-"`

	Memo string `json:"memo"`
	Type int `json:"type"`

	BillNo string  `json:"bill_no"`
	Amount float64  `json:"amount"`
	OrderId string `json:"order_id" orm:"column(orderId)"`
	Username string `json:"username" orm:"column(username)"`
	CreateTime  time.Time `json:"createtime" orm:"column(createtime)"`
	//UpdateTime  time.Time `json:"updatetime"  orm:"column(updatetime)"`
}

func (a *Tx) TableName() string {
	return "accountdetail"
}

func (t *Tx) Insert(sWid string) (n int64, err error) {
	o:=orm.NewOrm()
	o.Using(sWid)
	n, err = o.Insert(t)
	fmt.Println(n, err)
	return n, err
}
func  (t *Tx) MeronTx(sWid string)bool {
	o:= orm.NewOrm()
	o.Using(sWid)
	return o.QueryTable("accountdetail").Filter("bill_no", t.BillNo).Exist()
}

func MeronBetTx(sWid string, orderId string)bool {
	o:= orm.NewOrm()
	o.Using(sWid)
	return o.QueryTable("accountdetail").Filter("bill_no", "BT"+orderId).Exist()
}
func MeronBonusTx(sWid string, orderId string)bool {
	o:= orm.NewOrm()
	o.Using(sWid)
	return o.QueryTable("accountdetail").Filter("bill_no", "BN"+orderId).Exist()
}
func ChangeAct(iType int,  order * Order, act *Account ) (*Account, error){
	var deltaAmt float64
	var bonusAmt float64
	deltaAmt = 0
	bonusAmt = 0

	switch iType{
	case TX_TYPE_BET:
		deltaAmt = -1 * order.Amount
	case TX_TYPE_BONUS:
		deltaAmt = order.BonusAmount
		bonusAmt = order.BonusAmount
	case TX_TYPE_EXTRA:
		deltaAmt = order.ExtraBonus
		bonusAmt = order.ExtraBonus
	case TX_TYPE_CANCEL:
		if !MeronBetTx(order.Wid, order.OrderNo){
			err := errors.New("NO BET when wanna cc")
			return nil, err
		}
		deltaAmt = order.Amount
	default:
		err := errors.New("ERRR TYPE")
		return  nil, err
	}


	act.Amount +=deltaAmt
	act.BonusAmount +=bonusAmt
	return act , nil
}
//105 bonus
//201 bet deduct
// type amount, orderid
func NewOrderTx(userId uint64, iType int,  order * Order, act *Account ) (*Tx, error){
	object:=Tx{
		Amount:order.Amount,
		Type:iType,
		OrderId: order.OrderNo,
		Userid:order.Userid,
		Username:order.Username,
		CreateTime:time.Now(),
	}

	switch iType{
	case TX_TYPE_BET:
		object.AfterAmount = act.Amount
		object.BeforeAmount = act.Amount + order.Amount
		object.BillNo = "BT" + order.OrderNo
		object.Memo = MEMO_BET

	case TX_TYPE_BONUS:
		object.BillNo = "BN" + order.OrderNo
		object.Amount =   order.BonusAmount
		object.Memo = MEMO_BONUS

		object.AfterAmount =act.Amount
		object.BeforeAmount = act.Amount  - object.Amount

		object.AfterBonusAmount = act.BonusAmount
		object.BeforeBonusAmount = act.BonusAmount -object.Amount


	case TX_TYPE_EXTRA:
		object.BillNo = "EX" + order.OrderNo
		object.Amount =  order.ExtraBonus
		object.Memo = MEMO_EXTRA

		object.AfterAmount = act.Amount
		object.AfterBonusAmount = act.BonusAmount

		object.BeforeAmount = act.Amount - object.Amount
		object.BeforeBonusAmount = act.BonusAmount - object.Amount

	case TX_TYPE_CANCEL:
		object.BillNo = "CC" + order.OrderNo
		object.Memo = MEMO_CANCEL
		object.Amount = order.Amount

		object.AfterAmount =  act.Amount
		object.BeforeAmount =act.Amount - object.Amount

	default:
		err := errors.New("ERRR TYPE")
		return nil, err
	}
	return &object,  nil
}