package models

import (
	"time"
)

type UserSession struct {
	Wid         string    `json:"wid"`
	Sid         string    `json:"sid"`
	Username    string    `json:"username"`
	UserId      uint64    `json:"userId"`
	Balance     float64   `json:"balance"`
	RefreshTime time.Time `json:"refreshTime"`
	Code        string
	Description string
}

type RefreshMsg struct {
	Username string  `json:"username"`
	Balance  float64 `json:"balance"`
}
//{"code":"1","wid":"t2","description":"Success!","userId":54036,"username":"AI192"}

type UserInfo struct {
	//"username":"AI19",
	//"wid":"t2",
	//"id":53844
	Username string `json:"username"`
	Wid      string `json:"wid"`
	UserId   uint64 `json:"userId"`

	//"dbid":"t2",
	//"agentid":0,	"agentname":"",	//"class":"com.sby.bean.Userinfo",
	//"createtime":1523945418000,
	//"forwardOpen":0,"id":53844,"info":"",
	//"locktime":null,"loginCount":239761,
	//"loginIp":"139.162.23.175","merchantId":null,
	//"password":"46cc468df60c961d8da2326337c7aa58","paypasswd":null,
	//"rebate_info":"0.0|0.0|1","status":1,"token":"1978606b-8512-484e-aca5-66d341e2fe6c","type":1,"updatetime":1526049085279,"upuNames":"admin",
	//"withdrawCount":5}
}
