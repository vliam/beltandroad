
package models

import (
)
import (
	"encoding/json"

	"github.com/astaxie/beego"
)

type MsgBase struct {
	MesId      int `json:"mesId,omitempty"`
	SessionId  string`json:"sessionId,omitempty"`
}
type MsgTick struct {
	MesId      int `json:"mesId,omitempty"`
	Data struct {
		//tick
		 GameTick int   `json:"gameTick,omitempty"`
		 Odds float64 `json:"odds,omitempty"`
		 }`json:"data,omitempty"`
}
type MsgStop struct {
	MesId      int `json:"mesId,omitempty"`
	Data struct {
			   //stopp_at
			   StoppedAt float64   `json:"stopped_at,omitempty"`
			   Amount float64 `json:"amount,omitempty"`
			   Username string `json:"username,omitempty"`
			   Hash string `json:"hash,omitempty"`
		   }`json:"data,omitempty"`
}
type MsgStarting struct {
	MesId      int `json:"mesId,omitempty"`
	Data struct {
			   //starting
			   MaxWin float64   `json:"maxWin,omitempty"`
			   Issue uint64 `json:"issue,omitempty"`
			   TimeTillStart int `json:"timeTillStart,omitempty"`
		   }`json:"data,omitempty"`
}
/////////////////////////////////////////////////////
type MsgCrash struct {
	MesId      int `json:"mesId,omitempty"`
	CrashData struct {
			  Elapsed int   `json:"elapsed,omitempty"`
			   GameCrash float64 `json:"gameCrash,omitempty"`
			   Forced bool `json:"forced,omitempty"`
			   PoolSize int `json:"pool,omitempty"`
			   Bonuses map[string]float64 `json:"bonuses,omitempty"`
		   }`json:"data,omitempty"`
}
///////////////////////////////////////////////
type MsgStarted struct {
	MesId      int `json:"mesId,omitempty"`
	StartedData struct {
			  Issue uint64 `json:"issue,omitempty"`
			   MaxAmount float64 `json:"maxAmount,omitempty"`
			  Bets []Order `json:"bets,omitempty"`
			   //Bets map[string]float64 `json:"bets,omitempty"`
		   }`json:"data,omitempty"`
}
//{"data":{"gameTick":16774,"odds":"3.61"},"mesId":35}//tick
//{"data":{"amount":10,"stopped_at":2,"username":"t2_frank"},"mesId":32}
//{"data":{"elapsed":20582,"gameCrash":"4.36","forced":true,"bonuses":{}},"mesId":39}//crashed
//{"data":{"issue":"2018031402252","maxWin":"0","timeTillStart":5},"mesId":42}//starting
//{"data":{"issue":"2018031402252","bets":{},"maxAmount":0},"mesId":31}//started

//create a Tick msg
func NewTickMsg( iTime int , iOdds int ) string {
	msg:= MsgTick{MesId: MSG_PUB_GAME_TICK}
	//msg.Data.GameTick = iTime
	msg.Data. Odds = float64(iOdds)/100
	s , _:= json.Marshal(msg)
	return string(s)
}
func NewStopMsg( amount float64, username string, odds float64 , sHash string) string {
	msg:= MsgStop{MesId: MSG_PUB_STOP_AT}
	msg.Data.Amount = amount
	msg.Data.Username = username
	msg.Data.StoppedAt = odds
	msg.Data.Hash = sHash
	if odds <=1 {
		beego.Error("Bad Odds")
	}
	s , _:= json.Marshal(msg)
	return string(s)
}
func NewStartingMsg(issue uint64,  maxWin float64 ) string {
	msg:= MsgStarting{MesId: MSG_PUB_STARTING}
	//, Data:{   MaxWin: maxWin,Issue:issue,  TimeTillStart :5}
	//todo: getMaxWin
	msg.Data.MaxWin = maxWin
	msg.Data.Issue = issue
	msg.Data.TimeTillStart =5
	s , _:= json.Marshal(msg)
	return string(s)
}

func NewCrashMsg(elapsed int, odds float64 , bonus []Order, poolSize int ,  ) string {
	/*
	  Elapsed int   `json:"elapsed,omitempty"`
	   Amount float64 `json:"amount,omitempty"`
	   Forced bool `json:"forced,omitempty"`
	*/
	msg:= MsgCrash{MesId: MSG_PUB_CRASHED}
	//, Data:{  GameCrash:odds,Elapsed:elapsed, Forced:false  }
	msg.CrashData.Elapsed = elapsed
	msg.CrashData.GameCrash = odds
	msg.CrashData.Forced = false
	msg.CrashData.PoolSize = poolSize
	s , _:= json.Marshal(msg)
	return string(s)
}

func NewStarted(issue uint64,  maxAmount float64, bets []Order ) string {
	// bets here
	msg:= MsgStarted{MesId:MSG_PUB_STARTED}
	// Data:{   maxAmount: maxAmount,Issue:issue,Bets:{}}
	msg.StartedData.MaxAmount = maxAmount
	msg.StartedData.Issue = issue
	msg.StartedData.Bets = bets
	//msg.StartedData.Bets = make(map[string]float64)
	//fmt.Println(msg)
	s , _:= json.Marshal(msg)
	return string(s)
}
