package models

import (
)
import (
	"encoding/json"


)

const (
	MSG_PUB_GAME_TICK = 35
	MSG_PUB_STOP_AT = 32
	MSG_PUB_CRASHED = 39
	MSG_PUB_STARTING = 42
	MSG_PUB_STARTED = 31

	MSG_USER_BET = 4224
	MSG_PRI_BET_OK = 41
    //{"data":{"code":"0001","description":"Invalid parameter!  odds::2000.0   maxOdds::20.0"},"mesId":41}
	MSG_USER_CASH_OUT = 422
	MSG_USER_CASH_OUT_RETURN = 423

	MSG_USER_CANCEL = 34
	MSG_PRI_CANCEL_OK = 33
	MSG_PRI_CANCEL_ERR = 340

	MSG_USER_JOIN = 30
	MSG_PRI_WELCOME = 301
)
//{"mesId":4224,"sessionId":"33CB24669151D28A034F6AF16A326A45",
// "data":{"lotteryCode":"BOMB","rate":2,"amount":10,"currency":"USD"}}
//>>>{"data":{"amount":10,"issue":"2018031402264","index":2,"username":"frank"},"mesId":41}
//{"mesId":34,"sessionId":"33CB24669151D28A034F6AF16A326A45","data":{"name":"cancel_bet"}}
//>>>{"data":{"code":1},"mesId":33}
//{"mesId":422,"sessionId":"33CB24669151D28A034F6AF16A326A45","data":{"cash_out":""}}
type MsgCmd struct {
	MesId      int `json:"mesId,omitempty"`
	SessionId  string`json:"sessionId,omitempty"`
	BetData struct {
			Username string `json:"username"`
			Issue string `jsong:"issue,omitempty"`
			LotteryCode string   `json:"lotteryCode,omitempty"`
			Rate float64 `json:"rate,omitempty"`
			Amount float64 `json:"amount,omitempty"`
			Currency string `json:"currency,omitempty"`
			CashOut float64 `json:"cash_out,omitempty"`
		}`json:"data,omitempty"`
	Username   string    `json:"-"`
	Userid uint64 `json:"-"`
	Wid string `json:"-"`
}
type MsgReturn struct {
	MesId      int `json:"mesId,omitempty"`
	ReturnData struct {
			   Amount float64 `json:"amount,omitempty"`
			   Issue uint64 `json:"issue,omitempty"`
			   Code string `json:"code,omitempty"`
			   Index int `json:"index,omitempty"`
			   Username string `json:"username,omitempty"`
			   Description string `json:"description,omitempty"`
		   }`json:"data,omitempty"`
}
type MsgWelcome struct {
	MesId      int `json:"mesId,omitempty"`
	Data struct {
			   Issue uint64 `json:"issue,omitempty"`
			   Status int `json:"status,omitempty"`
			   HaveBet string `json:"have_bet,omitempty"`
			   CanCashOut string `json:"can_cash_out,omitempty"`
			   Bets []Order `json:"bets,omitempty"`
			   /* {
			   order.username,
			   order.amount,
			   order.odds
			   }*/
		   }`json:"data,omitempty"`
}

//create a return msg
func NewReturnMsg(order *Order, mesId int, sDescription string, sCode string) string {
	msg:= MsgReturn{MesId:mesId}
	if order!=nil{
		//"amount":10,"issue":"2018031402264","index":2,"username":"frank"
		msg.ReturnData.Amount = order.Amount
		msg.ReturnData.Username = order.Username
		msg.ReturnData.Issue = order.Issue
		msg.ReturnData.Description = sDescription
	}else{
		msg.ReturnData.Description = sDescription
		msg.ReturnData.Code = sCode
	}
	s , _:= json.Marshal(msg)
	return string(s)
}