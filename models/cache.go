package models


func AddValue(sKey string, dValue float64 )  {
	iValue := int (dValue*100)
	if !bm.IsExist(sKey){
		bm.Put(sKey, iValue , 0)
	}else{
		v := bm.Get(sKey).(int)
		bm.Put(sKey, v + iValue , 0)
	}
}

func GetValue(sKey string)float64  {
	var v int
	v= 0
	if !bm.IsExist(sKey){
		AddValue(sKey , 0)
		return 0
	}else{
		v = bm.Get(sKey).(int)
		return float64(v)/100
	}
}

func ClearValue(sKey string)  {

	if bm.IsExist(sKey){
		bm.Delete(sKey)
	}
}