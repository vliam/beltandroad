package mymemcache

import (
	"fmt"

	"strings"

	"github.com/astaxie/beego/cache"
	_ "github.com/astaxie/beego/cache/memcache"
	"github.com/astaxie/beego"
)

var bm cache.Cache

type UserInfo struct {
	Username string `json:"username"`
	Class    string `json:"class"`
	Dbid     string `json:"dbid"`
	Type     int64  `json:"type"`
}

/*
{
	"reflushGameNumJSB_01":20171024024,
	"reflushGameNumcf_01":20171016064,
	"reflushGameNumrb_01":20171024024,
	"userinfo":{
	"agentid":0,"agentname":"",
	"class":"com.sby.bean.Userinfo",
	"createtime":1475744569000,
	"dbid":"t2",
	"forwardOpen":0,
	"id":53766,"info":"","locktime":null,
	"loginCount":197348,	"loginIp":"111.125.72.81",
	"merchantId":null,
	"password":"46cc468df60c961d8da2326337c7aa58",	"paypasswd":null,"rebate_info":"0.0|6.0|4.0","status":1,
	"token":"34fbcfdd-f1e2-40d6-aaaa-06f84e938c80","type":9,
	"updatetime":1508151596285,"upuNames":"","username":"frank",
	"wid":"t2","withdrawCount":5},
	"account":{
		"amount":33704.4,"bonusAmount":15334.15,"class":"com.sby.bean.Account",
		"createtime":1475744569000,"freezeAmount":0.0,"id":52990,"presentAmount":0.0,
		"rechargeAmount":8087.56,"score":0,"status":0,"updatetime":1507190552000,
		"userid":53766,"username":"frank"
		}
}

*/
/*
func Close() {
	pool.Close()
}
*/
/*

WcO▒&cO▒11cO▒UcO▒K#630684D6418364C7E6DCEB5A45F3D9CF-n1{"userinfo":{"agentid":0,"agentname":"","class":"com.sby.bean.Userinfo","createtime":1523945418000,"dbid":"t2","forwardOpen":0,"id":53844,"info":"","locktime":null,"loginCount":239761,"loginIp":"139.162.23.175","merchantId":null,"password":"46cc468df60c961d8da2326337c7aa58","paypasswd":null,"rebate_info":"0.0|0.0|1","status":1,"token":"1978606b-8512-484e-aca5-66d341e2fe6c","type":1,"updatetime":1526049085279,"upuNames":"admin","username":"AI19","wid":"t2","withdrawCount":5},"account":{"amount":102497.2,"bonusAmount":217157.2,"class":"com.sby.bean.Account","createtime":1523945478000,"freezeAmount":0.0,"id":53074,"presentAmount":0.0,"rechargeAmount":0.0,"score":0,"status":0,"updatetime":1526037566000,"userid":53844,"username":"AI19"}}
*/
func GetJSONFromCache(sSession string) string {
	iStart := strings.Index(sSession, "\"userinfo\":{")
	iEnd := strings.Index(sSession, ",\"account\":{")
	return sSession[iStart+11 : iEnd]
}

func init() {
	var err error
	bm, err = cache.NewCache("memcache", `{"conn":"127.0.0.1:11211"}`)
	if err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(bm)
	}
	//d:=GetSession(beego.AppConfig.String("testSid"))
	//	fmt.Println(d)
} /*
func GetValueMem(key string) string {
	//bm, _ = cache.NewCache("memcache", `{"conn":"127.0.0.1:11211"}`)
	value := bm.Get(key).(string)
	fmt.Println(value)
	if len(value) > 0 {
		return value
	} else {
		return ""
	}
}
*/
func GetUserInfo(key string) string {
	//bm, _ := cache.NewCache("memcache", `{"conn":"127.0.0.1:11211"}`)
	v := bm.Get(key)
	if v != nil {
		if bm.Get(key) == nil {
			return ""
		}
		value := string(bm.Get(key).([]uint8))

		iStart := strings.Index(value, "\"userinfo\":{")
		iEnd := strings.Index(value, ",\"account\":{")
		if iEnd < iStart +11 {
			beego.Error(value)
			return ""
		}
		return value[iStart+11 : iEnd]
	} else {
		return ""
	}
}
