package models

import "time"

type GameStatus struct{
	GameId uint64
	GameStatus int
	GameRate int
	OpenGameId uint64
	TimeStarted time.Time
}
