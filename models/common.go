package models

import (
	"fmt"
	"strconv"

	"github.com/astaxie/beego"
	"github.com/astaxie/beego/cache"
	"github.com/astaxie/beego/orm"
	"github.com/astaxie/beego/utils"
	"strings"
)

var oDefault orm.Ormer
var bOrm map[string]bool
var MRed *utils.BeeMap //线程安全的map
var bm cache.Cache
var o orm.Ormer
var CallBackQue = make(chan *Order, 4095)

// ControllerError is controller error info structer.
type ControllerError struct {
	Status   int    `json:"status"`
	Code     int    `json:"code"`
	Message  string `json:"message"`
	DevInfo  string `json:"dev_info"`
	MoreInfo string `json:"more_info"`
}

func init() {
	//注册mysql Driver
	maxIdle := 500
	maxConn := 1000
	orm.RegisterDriver("mysql", orm.DRMySQL)
	sUrl := beego.AppConfig.String("mysql::url")
	dbuser := beego.AppConfig.String("mysql::user")
	dbpassword := beego.AppConfig.String("mysql::password")
	db := beego.AppConfig.String("mysql::dbname")
	conn := dbuser + ":" + dbpassword + "@tcp(" + sUrl + ")/" + db +
		"?charset=utf8&loc=Asia%2FShanghai"
	orm.RegisterModel(new(Issue), new(Account), new(Order), new(Tx))
	orm.RegisterDataBase("default", "mysql", conn, maxIdle, maxConn)
	/////////////////////////////////////////////
	bOrm = make(map[string]bool, 10)

	dbList := strings.Split(beego.AppConfig.String("mysql::dbList"), "|")
	fmt.Println(dbList)
	for i:=0 ; i <len(dbList); i++{
		aConfigTmp := strings.Split(dbList[i], ",")
		fmt.Println("DB",i,aConfigTmp)
		dbId:=aConfigTmp[0]
		conn:=dbuser + ":" + dbpassword + "@tcp(" + sUrl + ")/" + aConfigTmp[1] +
			"?charset=utf8&loc=Asia%2FShanghai"
		orm.RegisterDataBase(dbId, "mysql", conn, maxIdle, maxConn)
		bOrm[dbId] = true
	}
	///////////////////////////////////////////////////////////////////////
	fmt.Println("init models", sUrl)
	oDefault = orm.NewOrm()

	bm, _ = cache.NewCache("file", `{"CachePath":"./cache","FileSuffix":".cache","DirectoryLevel":2,"EmbedExpiry":120}`)
	//bm.Put("key", "somevalue", 0)
	/*//////////////////////////////////
	MRed = utils.NewBeeMap()
	MRed.Set("0", 1.0)
	MRed.Set("1", 2.1)
	MRed.Set("2", 3.0)
	MRed.Set("3", 4.5)
	MRed.Set("4", 5.0)
	MRed.Set("5", 6.0)
	MRed.Set("6", 7.0)
	MRed.Set("7", 8.0)
	MRed.Set("8", 9.4)
	MRed.Set("9", 10.5)
	beego.Debug("reinit mred")*/
	o = orm.NewOrm()
}

// Predefined model error codes.
const (
	ErrDatabase = -1
	ErrSystem   = -2
	ErrDupRows  = -3
	ErrNotFound = -4
)

// CodeInfo definiton.
type CodeInfo struct {
	Code int    `json:"code"`
	Info string `json:"description"`
}

/*/ CodeInfo definiton.
type UserInfo struct {
	Code int    `json:"code"`
	Username string    `json:"username"`
	Token string `json:"token"`
}
// return a UserInfo represents OK.
func NewUserInfo(username string, token string) *UserInfo {
	return &UserInfo{1, username, token}
}
*/
// NewErrorInfo return a CodeInfo represents error.
func NewErrorInfo(info string) *CodeInfo {
	return &CodeInfo{0, info}
}

// NewNormalInfo return a CodeInfo represents OK.
func NewNormalInfo(info string) *CodeInfo {
	return &CodeInfo{1, info}
}

//thread save map
func GetFromRed(id string) float64 {
	sIndex := id + "i"
	s := "0"
	if !MRed.Check(sIndex) {
		MRed.Set(sIndex, s)
	} else {
		s = MRed.Get(sIndex).(string)
		iS, err := strconv.Atoi(s)
		if err != nil {
			return -1
		} else {
			iS += 1
		}
		s = strconv.Itoa(iS)
		MRed.Set(sIndex, s)
	}
	if MRed.Check(s) {
		res := MRed.Get(s).(float64)
		MRed.Delete(s)
		return res
	} else {
		return -1
	}
}
