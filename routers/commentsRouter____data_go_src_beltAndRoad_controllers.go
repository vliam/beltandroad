package routers

import (
	"github.com/astaxie/beego"
)

func init() {

	beego.GlobalControllerRouter["beltAndRoad/controllers:WebSocketController"] = append(beego.GlobalControllerRouter["beltAndRoad/controllers:WebSocketController"],
		beego.ControllerComments{
			Method: "Join",
			Router: `/join`,
			AllowHTTPMethods: []string{"get"},
			Params: nil})

}
