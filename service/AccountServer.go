package service

import (
	"beltAndRoad/models"
	"beltAndRoad/models/mymemcache"
	"io/ioutil"
	"net/http"
	"time"

	"github.com/astaxie/beego"

	"encoding/json"

	"net"
	"strings"
	"sync"
)

var (
	err          error

	clientPool = new(sync.Map)
	LockBusySid = new(sync.Map)

	//req *http.Request
	netTransport *http.Transport
	reqErr error
	url string
	client *http.Client


	errInputData = &models.ControllerError{400, 10001, "数据输入错误", "客户端参数错误", ""}
	orderQue     [100]chan *models.Order
	oUserInfo *models.UserInfo
)

func  InitServ()  {
	netTransport = &http.Transport{
		Dial: (&net.Dialer{
			Timeout: 1* time.Second,
		}).Dial,
		DisableKeepAlives: true,
		TLSHandshakeTimeout: 2 * time.Second,
	}

	/*client =  &http.Client{
		Timeout: time.Second * 2,
		Transport: netTransport,
	}*/
	//respPool = new(sync.Map)

}
//在这里检查session id
//检查request中的sid，然后返回用户名
func RefreshSession(sid string) (u *models.UserSession, e *models.ControllerError) {
	//get session from memc
	//////////////////////////////
	sSession := mymemcache.GetUserInfo(sid)
	if len(sSession) < 5 {
		beego.Error("bad session memcached : ", sid)
		return nil, errInputData
	}
	var err error
	oUserInfo := new(models.UserInfo)
	err = json.Unmarshal([]byte(sSession), oUserInfo)
	if err != nil {
		beego.Error(err, sSession)
		return nil, errInputData
	}
	if oUserInfo.UserId > 0 {
		u = new(models.UserSession)
		u.RefreshTime = time.Now().UTC()
		u.Sid = sid
		u.Wid = oUserInfo.Wid
		u.UserId = oUserInfo.UserId
		u.Username = oUserInfo.Username
		return u, nil
	} else {
		return nil, errInputData
	}
}
//在这里检查session id
//检查request中的sid，然后返回用户名
func RefreshSessionHttp(sid string, iThread uint32) (u *models.UserSession, e *models.ControllerError) {
	var err error
	oUserInfo := new(models.UserInfo)

	LockBusySid.Store(iThread, true)
	q, _ := http.NewRequest("GET",
		beego.AppConfig.String("accountServer")+
			"/bomb/info/checkSession.do"	+
			";jsessionid="+sid,
		strings.NewReader(""))

	q.Header.Set("Connection", "close")

	if _,ok := clientPool.Load(iThread); !ok {
		clientPool.Store(iThread, &http.Client{
			Timeout: time.Second * 2,
			Transport: netTransport,
		})
	}

	v, _ := clientPool.Load(iThread)
	resp , err := v.(*http.Client).Do(q)

	if err != nil {
		beego.Error("ERR check session:", err)
		//return nil, errInputData
	}else{
		defer resp.Body.Close()
		var body []byte

		body, err = ioutil.ReadAll(resp.Body)

		if err == nil {
			err = json.Unmarshal(body, oUserInfo)
			if err != nil {
				beego.Error(string(body), err)
			}
		}else {
			beego.Error("Err_Read_Resp:", err)
		}
		resp.Body.Close()
		resp.Close = true
	}
	//finish do request
	/*respPool[iCsc].Body.Close()
	respPool[iCsc].Close = true*/
	LockBusySid.Delete( iThread  )
	netTransport.CloseIdleConnections()
	if err != nil {
		return nil, errInputData
	}

	if oUserInfo.UserId > 0 {
		u = new(models.UserSession)
		u.RefreshTime = time.Now().UTC()
		u.Sid = sid
		u.Wid = oUserInfo.Wid
		u.UserId = oUserInfo.UserId
		u.Username = oUserInfo.Username
		return u, nil
	} else {
		return nil, errInputData
	}
}
